<?php require "includes/session_inc.php"; ?>
<!DOCTYPE center PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<?php require "includes/initialize_inc.php";
	
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
	?>
		<script>
			location.href = "feedback.php";
		</script>
	<?php
		exit;
	}
	
	if (isset($_SESSION['thankYou'])) {
	
		$randRound = $_SESSION['thankYou'];
		
		$maxPayoutRounds = $_SESSION['maxDecksChoosenForPayout'];
		$roundsPerRepeat = 6;
		$numRepeats = $_SESSION['numberOfTestingRepeats'];
		$roundsOrder = array();
		for($i=0; $i<$numRepeats*$roundsPerRepeat; $i++){
			$roundsOrder[$i] = $i+1;
		}
		$payoutRounds = array($maxPayoutRounds);
		
		// shuffle the rounds. 
		for ($i = count($roundsOrder)-1; $i >= 0; $i--) {
			$j = mt_rand(0, $i);
			$temp = $roundsOrder[$i];
			$roundsOrder[$i] = $roundsOrder[$j];
			$roundsOrder[$j] = $temp;
		}
		
		// pick random rounds from the rounds order.
		for ($i = 0; $i < $maxPayoutRounds; $i++) {
			$payoutRounds[$i] = $roundsOrder[$i];
		}		
		
		// set the text to be displayed in the browser.
		$text = "The choices randomly selected for your bonus payment are as follows: \n";
	
		$reward = array();
		$rewardTotal = 0;
		$index = 0;
		$decision = array();
		foreach ($payoutRounds as $refPayoutRounds) {
			$sql = "SELECT round, decks, decision ".
					"FROM ".$_SESSION['tablePrefix']."responses ".
					"WHERE p_id='".$_SESSION['p_id']."' AND p_serial='".$_SESSION['p_serial']."' AND round=".$refPayoutRounds."";
			$result = mysql_query($sql) or die(mysql_error());
			$row = mysql_fetch_array($result);
			$round = $row['round'];
			$decks[$index] =  $row['decks'];
			$decision[$index] =  $row['decision'];
			if ($decision[$index] == 'A') {
				$input = $_SESSION['cardsOrderFromConfig']['cardOrderOfDeckA'];
				$reward[$index] = $input[mt_rand(0,count($input)-1)];
			}
			if ($decision[$index] == 'B') {
				$input = $_SESSION['cardsOrderFromConfig']['cardOrderOfDeckB'];
				$reward[$index] = $input[mt_rand(0,count($input)-1)];
			}
			if ($decision[$index] == 'C') {
				$input = $_SESSION['cardsOrderFromConfig']['cardOrderOfDeckC'];
				$reward[$index] = $input[mt_rand(0,count($input)-1)];
			}
			if ($decision[$index] == 'D') {
				$input = $_SESSION['cardsOrderFromConfig']['cardOrderOfDeckD'];
				$reward[$index] = $input[mt_rand(0,count($input)-1)];
			}
			if ($decision[$index] == 'E') {
				$input = $_SESSION['cardsOrderFromConfig']['cardOrderOfDeckE'];
				$reward[$index] = $input[mt_rand(0,count($input)-1)];
			}
			
			$text = $text . "<p>".($index+1).". <b>Pair".$decks[$index]."</b> <i>(Choice Pair=".$refPayoutRounds.")</i>. \n";
			$text = $text . "In that choice, you selected <b>Deck".$decision[$index]."</b>.";
			$text = $text . " The card you chose was worth <b>$".$reward[$index]."</b>. </p>";
			$rewardTotal = $rewardTotal + $reward[$index];
			$index++;
		}
		$text = $text . "<p><br/>This means that <b>$".$rewardTotal."</b> will be added as a bonus to your survey payment.";
		$text = $text . "<br/><br/>Please click �Next� to answer a couple of follow-up questions and then move on to the final, short section of the survey. </p>";
		
		$sql3 = "INSERT into ".$_SESSION['tablePrefix']."payout ".
				"(p_id, p_payout_round, p_payout_decision, p_payout_amount) ".
				"VALUES";
		$p_id = $_SESSION['p_id'];
		for ($i = 0; $i < $maxPayoutRounds; $i++) {
			$sql3 = $sql3." (".$p_id.", ".($i+1).", '".$decision[$i]."', $reward[$i])";
			if($i == $maxPayoutRounds-1){
				$sql3 = $sql3.";";
			} else {
				$sql3 = $sql3.",";
			}
		}
		mysql_query($sql3) or die(mysql_error());
		
		$sql2 = "UPDATE ".$_SESSION['tablePrefix']."participants ".
				"SET p_timeended=now(), p_total_time='".(time()-$_SESSION['taskStartTime'])."', ".
					"p_total_payout_amount=".$rewardTotal." ".
				"WHERE p_serial='".$_SESSION['p_serial']."' AND p_id='".$_SESSION['p_id']."' ";
		mysql_query($sql2) or die(mysql_error());	
	}
	
	?>
</head>

<body>

	<table class="outerTableLayout" align="center">
		<tbody><tr><td>
		
			<strong>Thank You</strong>
	
			<br/><br/>
			<?php echo $text;?>
			<br/><br/>
					
		 	<form name="mainform" id="mainform" method="post">
				<table width=100%>
					<tr>
						<td align=right width=100%>
							<input type=button id="nbutton" name="nbutton" value="Next" class="formButtons" 
								onClick="save_and_move2()" disabled />
							<input type=hidden id ='next' name='next' value=0 />
						</td>
					</tr>
				</table>
			</form>

		</td></tr></tbody>
	</table>
	
	<script language="javascript" type="text/javascript">
		history.forward();
		document.getElementById('nbutton').disabled=false;
	</script>
	
</body>

</html>
