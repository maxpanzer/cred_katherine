<?php require "includes/session_inc.php"; ?>
<?php 
	require "includes/initialize_inc.php";
	include $CFG["doc_root"]."/includes/header.inc.php";
?>
<h3>Cookies Required</h3>
Your web browser does not have cookies enabled. Cookies are required to complete this study. For instructions on how to enable cookies, <a href='cookie_help.html' target='_blank'>click here</a>. 
After you have enabled cookies, please <a href='<?=$_GET['redirect'];?>'>click here</a> to restart the study.
<br/><br/>
<?php include $CFG["doc_root"]."/includes/footer.inc.php"; ?>