<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<?php 
 	require "includes/initialize_inc.php";
	$_SESSION['cookietest'] = true;
	
	if (array_key_exists('previous', $_POST) && $_POST['previous'] > 0) {
	?>
		<script>
			window.location.href="training6.php";
		</script>
	<?php
		exit;
	}
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
	?>
		<script>
			window.location.href="survey.php";
		</script>
	<?php
		exit;
	}
	?>

<head>
</head>

<body>

<form id="mainform" name="mainform" method="post">
	<table class="outerTableLayout" align="center">
		<tbody class="deckLayout">
			<tr><td style="text-align:justify">
				<strong>Moving on!</strong>
			</td></tr>
			<tr>
				<td style="text-align:justify">
					If you would like to go back to refresh your memory about any previous decks, you may do so now by clicking 
					the "Previous" button below <font color="red">(NOT the Back button on your internet browser!)</font>.  
					<br/><br/>
					Once you feel like you have a good sense of the outcomes that each deck gives you, 
					click �Next� below to move on to the next section of the study.
					<br/><br/>
					Remember, <b>your payment for this study may depend on how familiar you are with these decks,
					</b> so do not move on until you are sure you are ready!
				</td>
			</tr>
			 
			<tr>
				<td>
				</td>
			</tr>  
			
			<tr>
				<td>
 				</td>
			</tr>
			
			<tr>
				<td>
					<input type=button id="pbutton" name="pbutton" value="Previous" class="formButtons" onClick="save_and_move()" disabled />
					<input type=button id="nbutton" name="nbutton" value="Next" class="formButtons" onClick="save_and_move2()" disabled />
					<input type=hidden id='clicks' name='clicks' value=0 />
					<input type=hidden id='previous' name='previous' value=0 />
					<input type=hidden id='next' name='next' value=0 />
				</td>
			</tr>
			
		</tbody>
	</table>
	</form>

	<script language="javascript" type="text/javascript">
		history.forward();
		document.getElementById('pbutton').disabled=false;
		document.getElementById('nbutton').disabled=false;
	</script>

</body>

</html>