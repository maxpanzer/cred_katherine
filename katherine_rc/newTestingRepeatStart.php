<?php require "includes/session_inc.php"; ?>
<?php 
	require "includes/initialize_inc.php";
	include "DeckPair.php";
	
	DeckPair::setAllDeckPairs($_SESSION['allowedDeckPairs']);
	$_SESSION['deckOrder'] = DeckPair::shuffleDeckPairs();	
	$_SESSION['deckOrderIndex'] = DeckPair::getShuffledDeckPairsIndex();
	
	// if this is the last testingRepeat then do not add anything to the database 
	if( $_SESSION['numTestingRepets'] != $_SESSION['numberOfTestingRepeats'] ) {
		$deckOrderString = DeckPair::getDeckPairsAsString($_SESSION['deckOrder']);
		$sql = "UPDATE ".$_SESSION['tablePrefix']."participants ".
				"SET version=CONCAT(version,'".$deckOrderString."') ".
				"WHERE p_serial='".$_SESSION['p_serial']."' AND p_id='".$_SESSION['p_id']."'";
		mysql_query($sql) or die(mysql_error());
	}
	
	$_SESSION['newTestingRepets'] = true;
	
	$_SESSION['newRound'] = true;
	$_SESSION['roundNumber'] = 0;
?>
	<script type="text/javascript">
		location.href = 'testingTL.php';
	</script>