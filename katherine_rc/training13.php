<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<?php
 	require "includes/initialize_inc.php";
	require "Thoughts.php";
		
	$practice = 'yes';
	
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		$thoughtIndex = $_SESSION['thoughtIndex'];
		$curThought = trim($_POST['curThought']);
		$_SESSION['thoughts'][$thoughtIndex] = $curThought;
		$thoughtIndex += 1;
		$_SESSION['thoughtIndex'] = $thoughtIndex;		
		$sql = "INSERT INTO ".$_SESSION['tablePrefix']."2_practice SET practice='$practice', p_id='".$_SESSION['p_id'].
				"', p_serial='".$_SESSION['p_serial']."', dateTime=now(), number=$thoughtIndex, duration='".
				(time()-$_SESSION['timestart'])."', input='$curThought'";
		mysql_query($sql) or die(mysql_error());
?>
	<script>
		//window.location.href="survey.php";
	</script>
<?php
	}
	
	// Start recording time when page opens.
	$_SESSION['timestart'] = time();
?>

	<script type="text/javascript">
		function goToSurvey(){
			window.location.href = "survey.php";
		}
	</script>

</head>

<body>

<form id="mainform" name="mainform" method="post">
	<table class="outerTableLayout" align="center">
		<tbody>
			<tr><td>
				<center><h3>Practice Listing Thoughts</h3></center>
			</td></tr>
			<tr><td>
				Please type in the name of the next number in the box below and, as soon as you are done, 
				hit the "Enter" key to submit your entry.
			</td></tr>
			<tr><td>
				<i>Don't worry too much about spelling, as we are mostly interested in what you are thinking.</i>			
			</td></tr>
			<tr><td style="text-align: justify">
				<table>
					<tr>
						<td width="30%"></td>
						<td width="40%">
							<p>Thought <?php echo ($_SESSION['thoughtIndex']+1); ?>:</p>
							<textarea 
								id="curThought" name="curThought" rows=5 cols=50 
								onKeyDown="limitText(document.mainform.curThought,document.mainform.countdown,200);" 
								onKeyUp="limitText(document.mainform.curThought,document.mainform.countdown,200); clickCheck();" 
								onKeyPress="return isNumberKey(event);" ></textarea>
							<br/>
							<p style="text-align: center">
							<font size="1">
								(Maximum of 200 characters per thought.)
								<br/>
								You have 
									<input readonly type="text" name="countdown" size="3" value="200"> 
								characters left.
							</font>
							</p>
						</td>
						<td width="30%"></td>
					</tr>
				</table>
				<br>
				
			</td></tr>
			<tr><td>
				<input type="hidden" id="next" name="next" value="0" />
				If you have entered the names of all of the numbers up to the number "five",
				click 
					<input type="button" name="survey" value="HERE" onclick="goToSurvey()" class="formButtons" />
			</td></tr>
			<tr><td>
			-------------------------------------------------------------------------------------------------------------------------
			</td></tr>
<?php 	
			if($_SESSION['thoughtIndex'] > 0) {
				echo "<tr><td>
					<b>So far, you have entered the following numbers:</b>
				  </td></tr>";
				for ($i = 0; $i < $_SESSION['thoughtIndex']; $i++) {
					echo "<tr><td>". 
							"Thought ".($i+1)." : ".$_SESSION['thoughts'][$i].
						 "</td></tr>";
				}
			} else {
				echo "<tr><td>
					<b>So far, you have NOT entered any numbers.</b>
				  </td></tr>";
			}
?>						
			
			<tr><td>
			
			</td></tr>
		</tbody>
	</table>
</form>
	
	<script language="javascript" type="text/javascript">
		document.mainform.curThought.focus();
		history.forward();
	</script>

</body>

</html>