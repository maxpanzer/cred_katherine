<?php 
//set php values
//error_reporting(E_ALL);
//ini_set("error_reporting",1);
//ini_set("max_execution_time",10);

/*
$currentSessionID = session_id();
echo "<br/> currentSessionID = ".$currentSessionID;
echo "<br/> GET = "; var_dump($_GET);
echo "<br/> POST = "; var_dump($_POST);
echo "<br/> SESSION = "; var_dump($_SESSION);
echo "<br/> COOKIE = "; var_dump($_COOKIE);
echo "<br/> REQUEST = "; var_dump($_REQUEST);
*/

//set up basic functionality: config, db, functions, classes...
require "config_inc.php";
require $CFG['doc_root']."/Deck.php";

if($CFG['isPD'] == true){
	$_SESSION['isPD'] = true;
	$_SESSION['tablePrefix'] = "pd_rc_";	
} else {
	$_SESSION['isPD'] = false;
	$_SESSION['tablePrefix'] = "rc_";
}

include $CFG['doc_root']."/includes/db_connect_inc.php";
include $CFG['doc_root']."/js/javascript_inc.php"; 
include $CFG['doc_root']."/css/css_inc.php";

//make sure they are secure
if ($CFG["require_https"] == true && $_SERVER['HTTPS'] != true) {
	echo 'Secure connection required.';
	exit;
}

//detect old browsers
$unsupported_browsers = array("msie 5","msie 4","msie 3","netscape 5","netscape 4","netscape 3");
foreach ($unsupported_browsers as $browser) {
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), $browser) !== false) { 
?>
	<script>
	window.location.href="oldbrowser.php";
	</script>
<?php
	exit;
	}
}


//save their serial ID and UIN if they have one
if (isset($_GET['serial']) && !isset($_SESSION['p_serial'])) {
	$_SESSION['p_serial'] = $_GET['serial'];
	$sql = "INSERT INTO ".$_SESSION['tablePrefix']."participants SET p_serial='".$_SESSION['p_serial'].
		   "', p_server_info=\"".addslashes(serialize($_SERVER))."\", p_timestarted=now()";
	mysql_query($sql) or die(mysql_error());
	$_SESSION['p_id'] = mysql_insert_id();
} else if (!isset($_GET['serial']) && !isset($_SESSION['p_serial'])) {
	$_SESSION['p_serial'] = 999;
	$sql = "INSERT INTO ".$_SESSION['tablePrefix']."participants SET p_serial='".$_SESSION['p_serial'].
		   "', p_server_info=\"".addslashes(serialize($_SERVER))."\", p_timestarted=now()";
	mysql_query($sql) or die(mysql_error());
	$_SESSION['p_id'] = mysql_insert_id();
}


//save the current experiment number from phpReferrer if it's set
if (isset($_GET['experiment_number'])) {
	$_SESSION['current_experiment'] = $_GET['experiment_number'];
} else if (isset($_GET['current_experiment'])) {
	$_SESSION['current_experiment'] = $_GET['current_experiment'];
}
if (isset($_GET['current_path'])) {
	$_SESSION['current_path'] = $_GET['current_path'];
}


if (isset($_GET['back'])) {
	$sql = "update ".$_SESSION['tablePrefix']."participants set p_".$_GET['back']."_backclicks = p_".$_GET['back'].
		   "_backclicks + 1 where p_id=".$_SESSION['p_id'];
	$db->query($sql); 
} elseif (isset($_GET['restart'])) {
	$sql = "update ".$_SESSION['tablePrefix']."participants set p_".$_GET['restart']."_restartclicks = p_".$_GET['restart'].
		   "_restartclicks + 1 where p_id=".$_SESSION['p_id'];
	$db->query($sql); 
}

if(!isset($_SESSION['isDeckInitialized']) || !$_SESSION['isDeckInitialized']){
	$cardsOrderLength = array();
	$CFG['cardsOrder']['cardOrderOfDeckA'] = preg_split('/,/', $CFG['cardsOrder']['cardOrderOfDeckA']);
	$CFG['cardsOrder']['cardOrderOfDeckB'] = preg_split('/,/', $CFG['cardsOrder']['cardOrderOfDeckB']);
	$CFG['cardsOrder']['cardOrderOfDeckC'] = preg_split('/,/', $CFG['cardsOrder']['cardOrderOfDeckC']);
	$CFG['cardsOrder']['cardOrderOfDeckD'] = preg_split('/,/', $CFG['cardsOrder']['cardOrderOfDeckD']);
	$CFG['cardsOrder']['cardOrderOfDeckE'] = preg_split('/,/', $CFG['cardsOrder']['cardOrderOfDeckE']);
	$_SESSION['cardsOrderFromConfig'] = $CFG['cardsOrder'];
}

$_SESSION['minimumNumCicksPerDeck'] = $CFG['minimumNumCicksPerDeck'];
$_SESSION['maxDecksChoosenForPayout'] = $CFG['maxDecksChoosenForPayout'];

$_SESSION['allowedDeckPairs'] = $CFG['allowedDeckPairs'];

$_SESSION['deckToListThoughts1'] = $CFG['deckToListThoughts1'];
$_SESSION['deckToListThoughts2'] = $CFG['deckToListThoughts2'];

$_SESSION['numberOfTestingRepeats'] = $CFG['numberOfTestingRepeats'];

?>
