<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<?php
	require "includes/initialize_inc.php";
	
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		session_destroy();
?>
		<script type="text/javascript">
			setTimeout('self.close()',500);
			document.write("Please Close this window, if not already closed.");
		</script>
<?php 
		exit;
	}
?>
<head>
	
	<script type="text/javascript">
	function closeMe()
	{
		this.focus();
		self.opener = this;
		self.close();
	}
	</script>
	
</head>

<body>
	<table class="outerTableLayout" style="padding:4em;">
		<tbody style="text-align:justify;">
		<tr><td>
			Thank you for participating!  
		</td></tr>
		<tr><td>
			<form name="mainform" method="post">
				Click <input type="button" onclick="save_and_move2()" id="nbutton" name="next" 
								class="formButtons" value="this" disabled> to close this window.	
				<input type="hidden" id="next" name="next" value="0">
			</form>
		</td></tr>
		</tbody>
	</table>
	
	<script type="text/javascript">
		history.forward();
		document.getElementById('nbutton').disabled=false;
	</script>
</body>

</html>