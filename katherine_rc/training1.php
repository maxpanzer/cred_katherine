<?php require "includes/session_inc.php"; ?>

<!DOCTYPE center PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<?php require "includes/initialize_inc.php";
	
	//we will check this value on the next page.
	$_SESSION['cookietest'] = true;
	$_SESSION['taskStartTime'] = time();
	
	Deck::getInstance()->generateDeckMap();
	Deck::getInstance()->initializeCardOrderMap();
	
	$_SESSION['deckOrder'] = Deck::getInstance()->getDeckmap();
	
	?>
	
	<script type="text/javascript">
		function goToNextPage(){
			location.href = 'training2.php';
		}
	</script>
</head>

<body>

	<table class="outerTableLayout" align="center">
		<tbody><tr><td>
		
			<strong>Card Game - Introduction</strong>
	
			<br/><br/>
			This section of the study will take you about 15 minutes.  Please make sure you have 
			15 minutes free, since you will perform better on this task <b>(and possibly earn more money)</b> 
			if you answer all the sections in one sitting.
			<br/><br/>
			In this first phase, you will learn about some different decks of cards.  
			The cards in the decks each have a dollar amount written on their other side.
			<br/><br/>
			The decks may differ in the <i>values</i> of the cards they contain, or <i>how many</i> of 
			each value of card they contain, or both.  
			<br/><br/>
			Later on, you will select cards from the different decks and the dollar values on 
			the cards you selected may affect how much you are paid for this study, so 
			<font color="red"><b>
				the better you remember how good each deck is, the more money you are likely to earn!
			</b></font> 
			You may spend as much time as you want getting familiar with the decks, 
			but please <b>do not write any deck information down</b> as you do.
			<br/><br/>
			
		 	<form>
				<table width=100%>
					<tr>
						<td align=right width=100%>
							<input type=button id="nbutton" value="Next" class="formButtons" 
								onClick="goToNextPage()" disabled />
						</td>
					</tr>
				</table>
			</form>

		</td></tr></tbody>
	</table>
	
	<script language="javascript" type="text/javascript">
		history.forward();
		document.getElementById('nbutton').disabled=false;
	</script>
	
</body>

</html>
