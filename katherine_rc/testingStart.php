<?php require "includes/session_inc.php"; ?>
<!DOCTYPE center PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<?php 
		require "includes/initialize_inc.php";
		include "DeckPair.php";		
		
		if(!isset($_SESSION['deckOrderTraining'])) {
			$_SESSION['deckOrderTraining'] = $_SESSION['deckOrder'];
		}

		DeckPair::setAllDeckPairs($_SESSION['allowedDeckPairs']);
		$_SESSION['deckOrder'] = DeckPair::shuffleDeckPairs();	
		$_SESSION['deckOrderIndex'] = DeckPair::getShuffledDeckPairsIndex();		
		
		$deckOrderString = DeckPair::getDeckPairsAsString($_SESSION['deckOrder']);
		$sql = "UPDATE ".$_SESSION['tablePrefix']."participants ".
				"SET version=CONCAT(version,'".$deckOrderString."') ".
				"WHERE p_serial='".$_SESSION['p_serial']."' AND p_id='".$_SESSION['p_id']."'";
		mysql_query($sql) or die(mysql_error());
		
		$_SESSION['counterbalance'] = -1;
		$_SESSION['numberTestingRoundsPerRepeat'] = count($_SESSION['deckOrder']);
		$_SESSION['roundNumber'] = 0;
		$_SESSION['globalRoundNumber'] = 0;
		$_SESSION['thankYou'] = mt_rand(0, count($_SESSION['deckOrder']));
		
		// reuse the thought variable in session.
		$_SESSION['thoughtIndex'] = 0;
		$_SESSION['thoughts'] = array();
		
		$_SESSION['newRound'] = true;
		$_SESSION['numTestingRepets'] = 0;
		$_SESSION['newTestingRepets'] = true;
		
	?>
	<script type="text/javascript">
		function goToNextPage(){
			location.href = 'testingTL.php';
		}
	</script>
</head>

<body>

	<table class="outerTableLayout" align="center">
		<tbody><tr><td>
		
			<strong>Card Game - Real Choices</strong>
	
			<br/><br/>
			You will now select a series of cards. You will see six pairs of decks,
			and you will be asked to choose one card from each pair. 
			<br/><br/>
			One or more of these six choices will be paid out to you in <font color="red"><b>real money</b></font>- that is,
			you will get paid the amount of money on the cards you chose for those randomnly selected pairs.
			<br/><br/>
			You will find out the value of those selected cards once you have made all six choices.
			<br/><br/>
					
		 	<form>
				<table width=100%>
					<tr>
						<td align=right width=100%>
							<input type=button id="nbutton" value="Next" class="formButtons" 
								onClick="goToNextPage()" disabled />
						</td>
					</tr>
				</table>
			</form>

		</td></tr></tbody>
	</table>
	
	<script language="javascript" type="text/javascript">
		history.forward();
		document.getElementById('nbutton').disabled=false;
	</script>
	
</body>

</html>
