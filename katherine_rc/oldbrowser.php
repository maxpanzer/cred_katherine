<?php require "includes/session_inc.php"; ?>
<?php 
	require "includes/initialize_inc.php";
	include $CFG["doc_root"]."/includes/header.inc.php";
?>
<h3>Newer Browser Required</h3>
Unfortunately, this study will not work with the browser you are using. You need to download one of the freely available, newer browsers in order to complete the study. 
You can download the latest version of Firefox at <a href='http://www.mozilla.com/en-US/firefox/'>http://www.mozilla.com/en-US/firefox/</a>. Alternately, if you are using a PC, you can download the latest version of Internet Explorer at <a href='http://www.microsoft.com/windows/ie/downloads/default.mspx'>http://www.microsoft.com/windows/ie/downloads/default.mspx</a>. We apologize for any inconvenience.
<br/><br/><br/><br/><br/>
<?php include $CFG["doc_root"]."/includes/footer.inc.php"; ?>