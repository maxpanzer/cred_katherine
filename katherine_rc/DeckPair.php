<?php
		
class DeckPair {
	
	private static $allDeckPairs = array();
	private static $allDeckPairsIndex = array();
	
	private static $shuffledDeckPairs = null;
	private static $shuffledDeckPairsIndex = array();
	
	public static function setAllDeckPairs($allDeckPairsFromSession) {
		$tempPairs = explode(",", $allDeckPairsFromSession);
		$index = 0;
		foreach ($tempPairs as $tPair) {
			self::$allDeckPairs[$index] = $tPair;
			self::$allDeckPairsIndex[$index] = $index;
			$index = $index + 1;
		}		
	}
	
	public static function getAllDeckPairs() {
		return self::$allDeckPairs;
	}
	
	public static function getShuffledDeckPairsIndex() {
		return self::$shuffledDeckPairsIndex;
	}
	
	public static function shuffleDeckPairs() {
		self::$shuffledDeckPairs = self::$allDeckPairs;
		self::$shuffledDeckPairsIndex = self::$allDeckPairsIndex;
		for ($i = count(self::$shuffledDeckPairs)-1; $i >= 0; $i--) {
			$j = mt_rand(0, $i);
			
			$temp = self::$shuffledDeckPairs[$i];
			self::$shuffledDeckPairs[$i] = self::$shuffledDeckPairs[$j];
			self::$shuffledDeckPairs[$j] = $temp;
			
			$tempIndex = self::$shuffledDeckPairsIndex[$i];
			self::$shuffledDeckPairsIndex[$i] = self::$shuffledDeckPairsIndex[$j];
			self::$shuffledDeckPairsIndex[$j] = $tempIndex;				
		}
		self::$shuffledDeckPairs = self::randomizePairs(self::$shuffledDeckPairs);
		return self::$shuffledDeckPairs;		
	}
	
	private static function randomizePairs($pairs) {
		for ($i = count($pairs)-1; $i >= 0; $i--) {
			$temp = $pairs[$i];			
			$j = mt_rand(0, 1);
			if($j == 0) {
				$tempAry = explode("-", $temp);		
				$pairs[$i] = $tempAry[1] . "-" . $tempAry[0];
			}
		}
		return $pairs;
	}
	
	public static function isThoughtsNeededForRound($ary, $index, $deckToListThoughts1, $deckToListThoughts2) {
		if($ary[$index] == $deckToListThoughts1 || $ary[$index] == $deckToListThoughts2) {
			return true;			
		}
		return false;
	}
	
	public static function getDeckPairsAsString($deckPairs) {
		$toReturn = "";
		for($i=0; $i<count($deckPairs); $i++) {
			if($i != count($deckPairs)-1) {
				$toReturn = $toReturn . $deckPairs[$i] . ",";
			} else {
				$toReturn = $toReturn . $deckPairs[$i] . ";";
			}
		}
		return $toReturn;
	}
}

?>