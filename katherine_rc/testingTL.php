<?php require "includes/session_inc.php"; ?>
<!DOCTYPE center PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<?php 
	require "includes/initialize_inc.php"; 
	include "DeckPair.php";
		
	if(isset($_SESSION['newTestingRepets'])) {
		$_SESSION['numTestingRepets'] = $_SESSION['numTestingRepets'] + 1;
		unset($_SESSION['newTestingRepets']);
		
		// since $_SESSION['numTestingRepets'] is already incremented above we nee to use '>' insteatd of '='
		if($_SESSION['numTestingRepets'] > $_SESSION['numberOfTestingRepeats']){
?>
			<script>
				window.location.href="thankYou.php";
			</script>
<?php 		
			exit;	
		}
	}
	
	if(isset($_SESSION['newRound'])) {
		
		if( $_SESSION['roundNumber'] == $_SESSION['numberTestingRoundsPerRepeat'] ) {
?>
			<script>
				window.location.href="newTestingRepeatStart.php";
			</script>
<?php
			exit;
		}
		
		$_SESSION['globalRoundNumber'] = $_SESSION['globalRoundNumber'] + 1;
		$globalRoundNumber = $_SESSION['globalRoundNumber'];
		$roundNumber = $_SESSION['roundNumber'];
		$roundNumber = $roundNumber + 1;
		$_SESSION['roundNumber'] = $roundNumber;
		unset($_SESSION['newRound']);
		
		// if thought listing is not necessary, then go to DrawCard page.
		if(!DeckPair::isThoughtsNeededForRound($_SESSION['deckOrderIndex'], 
												$roundNumber-1, 
												$_SESSION['deckToListThoughts1'], 
												$_SESSION['deckToListThoughts2'])) {
?>
			<script>
				window.location.href="testingDrawCard.php";
			</script>
<?php
			exit;
		}
	} 
	
	$globalRoundNumber = $_SESSION['globalRoundNumber'];
	$roundNumber = $_SESSION['roundNumber'];
	$deckOrder = $_SESSION['deckOrder'][($roundNumber-1)];
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		$thoughtIndex = $_SESSION['thoughtIndex'];
		$curThought = trim($_POST['curThought']);
		$_SESSION['thoughts'][$thoughtIndex] = $curThought;
		
		$sql = "INSERT INTO ".$_SESSION['tablePrefix']."2_responses ".
				"SET thoughtNumber=".($thoughtIndex+1).", p_id='".$_SESSION['p_id']."', ".
				"p_serial='".$_SESSION['p_serial']."', dateTime=now(), ".
				"decks='".$deckOrder."', duration=".(time()-$_SESSION['timestart']).
				", thought='".$curThought."'";
		mysql_query($sql) or die(mysql_error());
		$thoughtIndex += 1;
		$_SESSION['thoughtIndex'] = $thoughtIndex;
?>
		<script>
			window.location.href="testingTL.php";
		</script>
<?php
		
		exit;
	}
	
	$deckary = explode("-", $deckOrder);
	$left = $deckary[0];
	$right = $deckary[1];
	
	$_SESSION['timestart'] = time();
?>

	<script type="text/javascript">
		function goToNextPage(){
			window.location.href = "testingDrawCard.php";
		}
	</script>
</head>

<body>

<form name="mainform" method="post">
	
	<table class="outerTableLayout" style="padding:4em;">
		<tr><td>
			<?php echo $globalRoundNumber; ?>. Before you make your next choice, please consider the two decks below:
		</td></tr>
		<tr>
			<td style="text-align:justify">
				<table cellspacing='10' align="center">
					<tr>
						<td align="center">
							<img class="imgClass" src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining'][$left]; ?>.jpg" 
								alt='<?php echo "Deck".$left;?>' />
						</td>
						<td><B>OR</B></td>
						<td align="center">
							<img class="imgClass" src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining'][$right]; ?>.jpg" 
								alt='<?php echo "Deck".$right;?>' />
						</td>
					</tr>
					
					<tr valign="middle" align="center">
						<td>
							<b>Deck <?php echo $left ?></b>
						</td>
						<td></td>
						<td>
							<b>Deck <?php echo $right ?></b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	
		<tr><td>
			Please type your thoughts about these decks in the box below, one at a time. 
			As soon as you are done with each thought, hit the "Enter" key to submit it, 
			and then you will be able to type the next one.
		</td></tr>
		<tr><td>
			<br/>Note: You will make your actual choice in a moment--for now, please just type your 
			<b>thoughts about the choice</b> in the box below.
		</td></tr>
		
		<tr>
			<td style="text-align:left">
				<table cellspacing='10' align="center">
					<tr><td>
						Thought:
					</td></tr>
					<tr>
						<td width="60px" align="center">
							<textarea id="curThought" name="curThought" rows=5 cols=50 
								onKeyDown="limitText(document.mainform.curThought,document.mainform.countdown,200);" 
								onKeyUp="limitText(document.mainform.curThought,document.mainform.countdown,200); clickCheck();" 
								onKeyPress="return isNumberKey(event);"></textarea>
							<br/>
							<font size="1">(Maximum of 200 characters per thought.)
								You have 
								<input readonly type="text" name="countdown" size="3" value="200"> 
								characters left.
							</font>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr><td>
			If you have entered all of your thoughts about the choice between these decks 
			and are sure that you cannot think of anything else, 
			click  
				<input type="button" name="nextPage" value="HERE" onclick="goToNextPage()" class="formButtons" />
		</td></tr>
		
		<tr><td>
		-------------------------------------------------------------------------------------------------------------------------
		</td></tr>
<?php 	
			if($_SESSION['thoughtIndex'] > 0) {
				echo "<tr><td colspan=\"3\">
					<b>So far, you have entered the following thoughts:</b>
				  </td></tr>";
				for ($i = 0; $i < $_SESSION['thoughtIndex']; $i++) {
					echo "<tr><td colspan=\"3\">". 
							"Thought ".($i + 1)." : ".$_SESSION['thoughts'][$i].
						 "</td></tr>";
				}
			} else {
				echo "<tr><td colspan=\"3\">
					<b>So far, you have NOT entered any thoughts.</b>
				  </td></tr>";
			}
?>	
		<tr><td colspan="3">
		</td></tr>
	</table>
	
	<input type=hidden id='next' name='next' value=0 />
</form>	

<script language="javascript" type="text/javascript">
	document.mainform.curThought.focus();
	history.forward();
</script>

</body>

</html>
