<?php require "includes/session_inc.php"; ?>
<!DOCTYPE center PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<?php 
	require "includes/initialize_inc.php"; 
	
	$thoughtsLength = count($_SESSION['thoughts']);
	if(!isset($_SESSION['curThoughtIndex'])) {
		$curThoughtIndex = 0;
		$_SESSION['curThoughtIndex'] = $curThoughtIndex;
	} else {
		$curThoughtIndex = $_SESSION['curThoughtIndex'];
	}
	
	$globalRoundNumber = $_SESSION['globalRoundNumber'];
	$roundNumber = $_SESSION['roundNumber'];
	$deckOrder = $_SESSION['deckOrder'][$roundNumber-1];
	$primarily = array_key_exists('primarily', $_POST)? $_POST['primarily'] : -1;
	$valence = array_key_exists('primarily', $_POST) ? $_POST['valence'] : -1;
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		$sql = "UPDATE ".$_SESSION['tablePrefix']."2_responses ".
				"SET primarily='".$primarily."', valence='".$valence."', ".
				"dateTime2=now(), duration2='".(time()-$_SESSION['timestart'])."' ".
				"WHERE thoughtNumber='".($curThoughtIndex+1)."' AND p_id='".$_SESSION['p_id']."' AND decks='".$deckOrder."'";
		mysql_query($sql) or die(mysql_error());
		
		$curThoughtIndex = $curThoughtIndex + 1;
		$_SESSION['curThoughtIndex'] = $curThoughtIndex;
		
		if($curThoughtIndex < $thoughtsLength){
?>
			<script>
				window.location.href="testingTLThoughts.php";
			</script>
<?php
			exit;
		} else {
			unset($_SESSION['curThoughtIndex']);
			$_SESSION['thoughts'] = array();
			$_SESSION['thoughtIndex'] = 0;
			$_SESSION['newRound'] = true;
?>
			<script>
				window.location.href="testingTL.php";
			</script>
<?php			
			exit;
		}
	}
	
	$deckary = explode("-", $deckOrder);
	$left = $deckary[0];
	$right = $deckary[1];
	
	$_SESSION['timestart'] = time();
?>

	<script type="text/javascript">
		function enableNext(){
			var pLen = document.mainform.primarily.length;
			for (i = 0; i<pLen; i++) {
				if (document.mainform.primarily[i].checked) {
					pSelected = document.mainform.primarily[i].value;
				}
			}
			
			var vLen = document.mainform.valence.length;
			for (i = 0; i<vLen; i++) {
				if (document.mainform.valence[i].checked) {
					vSelected = document.mainform.valence[i].value;
				}
			}
			
			if(pSelected == "" || vSelected == ""){
				document.getElementById('nbutton').disabled = true;
			} else {
				document.getElementById('nbutton').disabled = false;
			}
		}
	</script>
</head>

<body>

<form name="mainform" method="post">
	
	<table class="outerTableLayout" style="padding:4em;">
		<tr><td>
			Thank you. Now, before you move on, we would like to ask you some questions about 
			each of the thoughts you listed about the two decks you just chose between. 
			We will present each thought individually and ask you about it.
		</td></tr>
		
<?php 	
		if($curThoughtIndex == 0){
			echo "<tr><td> <br/> Here is the first thought you typed </tr></td>";
		} 
		echo "<tr><td>
			<b>Thought ".($curThoughtIndex+1).": ".$_SESSION['thoughts'][$curThoughtIndex]."</b>
		  </td></tr>";
?>	
		
		<tr><td>
			<br/>
			Please review this thought, and answer the following:
		</td></tr>
		<tr><td>
			<table border="6" cellpadding="5px">
				<tr><td>
					1. Is this thought primarily about Deck <?php echo $left; ?> or Deck <?php echo $right; ?>?:
					<br/>
					<input type="radio" id="primarily" name="primarily" value="deck<?php echo $left; ?>" onclick="enableNext()" /> Deck <?php echo $left; ?><br />
					<input type="radio" id="primarily" name="primarily" value="deck<?php echo $right; ?>" onclick="enableNext()" /> Deck <?php echo $right; ?><br />
					<input type="radio" id="primarily" name="primarily" value="neither" onclick="enableNext()" /> Neither<br />
				</td></tr>
				<tr><td>
					2. Is this thought something <b>positive</b> about that deck, or <b>negative</b> about it?
					<br/>
					<input type="radio" id="valence" name="valence" value="+" onclick="enableNext()"/> Positive<br/>
					<input type="radio" id="valence" name="valence" value="-" onclick="enableNext()" /> Negative<br/>
					<input type="radio" id="valence" name="valence" value="neither" onclick="enableNext()" /> Neither<br/> 
				</td></tr>
			</table>
		</td></tr>
		
		<tr><td align="center">
			<br/>
			<input type=hidden id='next' name='next' value=0 />
			<input type=button id="nbutton" value="Next" style="float:left;" onclick="save_and_move2();" disabled />
		</td></tr>
	</table>
	
</form>	

	<script language="javascript" type="text/javascript">
		history.forward();
	</script>

</body>

</html>
