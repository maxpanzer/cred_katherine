<?php

/* This is the global config file for the whole script. When you edit the 
config variables here, be careful about adding double quotes ("). They must
always be preceded by a backslash (\), or else there will be a parse error.  */
global $CFG;

/* 
//MySQL database connection info
$CFG['db_host'] = "localhost";
$CFG['db_user'] = "katherinedb";
$CFG['db_pass'] = "b9absak2d";
$CFG['db_database'] = "katherine";
*/
$CFG['db_host'] = "localhost";
$CFG['db_user'] = "root";
$CFG['db_pass'] = "";
$CFG['db_database'] = "katherine";

$CFG['isPD'] = false;

//require SSL?
$CFG['require_https'] = false;

// web_root = the root dirs. If all the files are in the cct directory in the web root of your server, 
// these settings should be fine

/*
 * $CFG['web_root'] = "https://".$_SERVER['HTTP_HOST']."/act_exp/katherine_tl/";
 * $CFG['doc_root'] = $_SERVER['DOCUMENT_ROOT']."/act_exp/katherine_tl/";
 * $CFG['title'] = "Decisions"; //default site title
 * $CFG['finishpage'] = "https://".$_SERVER['HTTP_HOST']."/act_exp/katherine_tl/thankYou.php";
 */
$CFG['web_root'] = "http://".$_SERVER['HTTP_HOST']."/CRED_Katherine/katherine_tl/";
$CFG['doc_root'] = $_SERVER['DOCUMENT_ROOT']."CRED_Katherine/katherine_tl/";
$CFG['title'] = "Decisions"; //default site title
$CFG['finishpage'] = "http://".$_SERVER['HTTP_HOST']."/CRED_Katherine/katherine_tl/thankYou.php";

/*
 * Cards Configuration. 
 * Give the Order of the cards for Each Deck here. This will be used in determining the order in which the
 * cards are shown to the user. Kind a like a crude puseudo-random cards order generator. 
 * The final pay to the user will also depend on the values given here.
 */
$CFG['cardsOrder']['cardOrderOfDeckA'] = "0,0,0,0,4,0,0,0,0,4,0,0,0,0,4,0,0,0,0,4";
$CFG['cardsOrder']['cardOrderOfDeckB'] = "0,0,0,0,3,0,0,3,0,0,0,3,0,0,0,3,0,0,0,3";
$CFG['cardsOrder']['cardOrderOfDeckC'] = "0,0,0,0,0,0,0,0,0,0,0,0,3,4,0,0,0,3,4,0";
$CFG['cardsOrder']['cardOrderOfDeckD'] = "0,0,0,3,0,0,0,3,0,0,0,3,0,0,0,3,0,0,0,3";
$CFG['cardsOrder']['cardOrderOfDeckE'] = "0,0,0,0,4,0,0,0,0,4,0,0,0,0,4,0,0,0,0,4";

$CFG['minimumNumCicksPerDeck'] = "2";

// not used anymore.
$CFG['testingSetToSelect'] = "C";
	
$CFG['maxDecksChoosenForPayout'] = "4";

// all allowed deck pairs that show up in the testing phase. DO NOT CHANGE.
$CFG['allowedDeckPairs'] = "A-B,A-D,A-E,B-D,B-E,D-E";

// type the numbers of the deck which need to have thought listing.
// the numbers and the corresponding decks are given below.
// A-B: 0
// A-D: 1
// A-E: 2
// B-D: 3
// B-E: 4
// D-E: 5
$CFG['deckToListThoughts1'] = 0;
$CFG['deckToListThoughts2'] = 5;

?>