<?php require "includes/session_inc.php"; ?>
<!DOCTYPE center PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<?php 
		require "includes/initialize_inc.php";
		include "DeckPair.php";
		
		$counterbalance = $_SESSION['counterbalance'];
		$roundNumber = $_SESSION['roundNumber'];
		$deckOrder = $_SESSION['deckOrder'][$roundNumber-1];
		
		if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
			$decision = $_POST['decision'];
			$clicks = $_POST['clicks'];
			$sql = "INSERT INTO ".$_SESSION['tablePrefix']."tl_responses ".
					"SET p_id='".$_SESSION['p_id']."', p_serial='".$_SESSION['p_serial']."', ".
					"dateTime=now(), groups = 999, counterbalance=".$counterbalance.", ".
					"round=".$roundNumber.", decks='".$deckOrder."', decision='".$decision."', ".
					"judgmentDuration=".(time()-$_SESSION['timestart']).", button = 'next' ";
			echo $sql;
			mysql_query($sql) or die(mysql_error());
			
			// if thought listing is not necessary, then go to DrawCard page.
			if(!DeckPair::isThoughtsNeededForRound($_SESSION['deckOrderIndex'], $roundNumber-1, $_SESSION['deckToListThoughts1'], $_SESSION['deckToListThoughts2'])) {
				$_SESSION['newRound'] = true;
?>
				<script>
					window.location.href="testingTL.php";
				</script>
<?php
				exit;
			} else {
?>
		<script>
			window.location.href = "testingTLThoughts.php";
		</script>
<?php
				exit;	
			}	
		}
	
		$deckOrder = $_SESSION['deckOrder'][$roundNumber-1];
		$deckary = explode("-", $deckOrder);
		$left = $deckary[0];
		$right = $deckary[1];
?>
</head>

<body>

<form name="mainform" method="post">
	<table class="outerTableLayout" align="center">
		<tbody>
		
			<tr><td>
				<?php echo $roundNumber ?>. Please choose one deck to pull a card from:
			</td></tr>
			
			<tr>
				<td style="text-align:justify">
				<table cellspacing='10' align="center">
					<tr>
						<td align='center'><img class="imgClass" 
							src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining'][$left]; ?>.jpg" 
							alt='<?php echo "Deck".$left;?>' /></td>
						<td><B>OR</B></td>
						<td align='center'><img class="imgClass" 
							src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining'][$right]; ?>.jpg" 
							alt='<?php echo "Deck".$right;?>' /></td>
					</tr>
					<tr valign="middle" align="center">
						<td>
							<b>Deck <?php echo $left ?></b>
							<a id="leftRB" href="javascript:radioButtonClick('leftRB','<?php echo $left;?>','<?php echo $right;?>')">
								<img class="imgClassRadioButton" src="images/rb-uncheck.png" alt="RadioButtonImage">
							</a>
						</td>
						<td></td>
						<td>
							<b>Deck <?php echo $right ?></b>
							<a id="rightRB" href="javascript:radioButtonClick('rightRB','<?php echo $left;?>','<?php echo $right;?>')">
								<img class="imgClassRadioButton" src="images/rb-uncheck.png" alt="RadioButtonImage">
							</a>
						</td>
					</tr>
				</table>
				</td>
			</tr>
				
			<tr><td align="center">
				<input type=hidden id='clicks' name='clicks' value=0 />
				<input type=hidden id='next' name='next' value=0 />
				<input type=hidden id='decision' name='decision' value=0 />
				<input type=button id="nbutton" name="nbutton" value="Next" class="formButtons" 
					onClick="save_and_move2()" disabled />
			</td></tr>
			
			<tr><td>
			</td></tr>
		</tbody>
	</table>
</form>

	<script language="javascript" type="text/javascript">
		history.forward();
	</script>
	
</body>

</html>
