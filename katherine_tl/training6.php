<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<?php 
	 	require "includes/initialize_inc.php";
	$round = 5;
	$deck = 'e';
	
	
	if (array_key_exists('previous', $_POST) && $_POST['previous'] > 0) {
		$clicks = $_POST['clicks'];
		$button = "previous";
		$sql = "SELECT * FROM ".$_SESSION['tablePrefix']."tl_practice WHERE p_id=".$_SESSION['p_id'].
				" and round=".$round." and decks='".$deck."'";
		$result = mysql_query($sql) or die(mysql_error());
		if (mysql_num_rows($result) )
		{
			$row = mysql_fetch_array($result);
			$clicks += $row['clicks'];
			$duration = (time()-$_SESSION['timestart']) + $row['duration'];
			$sql = "UPDATE ".$_SESSION['tablePrefix']."tl_practice SET clicks=".$clicks.", duration='".
					$duration."' WHERE p_id=".$_SESSION['p_id']." and round=".$round." and decks='".$deck."'";
			mysql_query($sql);
		}
		else
		{
		    $sql = "INSERT INTO ".$_SESSION['tablePrefix']."tl_practice SET button = '".$button.
		    		"', p_id=".$_SESSION['p_id'].", p_serial=".$_SESSION['p_serial'].
		    		", dateTime=now(), round=$round, decks='$deck', clicks=$clicks, duration='".
		    		(time()-$_SESSION['timestart'])."'";
			mysql_query($sql);
		}
		?>
		<script>
			window.location.href="training5.php";
		</script>
		<?php
		exit;
	}
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		$clicks = $_POST['clicks'];
		$button = "next";
		$sql = "SELECT * FROM ".$_SESSION['tablePrefix']."tl_practice WHERE p_id=".$_SESSION['p_id']." and round=".
				$round." and decks='".$deck."'";
		$result = mysql_query($sql) or die(mysql_error());
		if (mysql_num_rows($result) )
		{
			$row = mysql_fetch_array($result);
			$clicks += $row['clicks'];
			$duration = (time()-$_SESSION['timestart']) + $row['duration'];
			$sql = "UPDATE ".$_SESSION['tablePrefix']."tl_practice SET clicks=".$clicks.", duration='".$duration.
					"' WHERE p_id=".$_SESSION['p_id']." and round=".$round." and decks='".$deck."'";
			mysql_query($sql);
		}
		else
		{
		    $sql = "INSERT INTO ".$_SESSION['tablePrefix']."tl_practice SET button = '".$button."', p_id=".
		    		$_SESSION['p_id'].", p_serial=".$_SESSION['p_serial'].
		    		", dateTime=now(), round=$round, decks='$deck', clicks=$clicks, duration='".
		    		(time()-$_SESSION['timestart'])."'";
			mysql_query($sql);
		}
		?>
		<script>
			window.location.href="training7.php";
		</script>
		<?php
		exit;
	}
	
	// Start recording time when page opens.
	$_SESSION['timestart'] = time();
	$a = 0;
	?>

<head>
	<?php 
		echo Deck::getInstance()->getCardOrderJavaScriptForRound($round);
		
		$scriptText = "var round=$round; \n";
		$sqlMinClicks = "SELECT clicks from ".$_SESSION['tablePrefix']."tl_practice ".
						"WHERE p_id=".$_SESSION['p_id']." and round=".$round." and decks='".$deck."'";
		$result = mysql_query($sqlMinClicks) or die(mysql_error());
		if (mysql_num_rows($result)) {
			$row = mysql_fetch_array($result);
			$clicks = $row['clicks'];
			$scriptText = $scriptText."var clickPerDeckCompleted=$clicks; ";
		} else {
			$scriptText = $scriptText."var clickPerDeckCompleted=0; \n";
		}
	?>
	<script language="javascript" type="text/javascript">
		<?php echo $scriptText; ?>
	</script>
</head>

<body>

<form id="mainform" name="mainform" method="post">
	<table class="outerTableLayout" align="center">
		<tbody class="deckLayout">
			<tr>
				<td style="text-align:justify">
					Here is Deck E.  
					<br/><br/>
					Click on it to test out the cards it gives you.
					<br/><br/>
					You may click as many times as you want, until you feel confident that you know how good Deck E is. 
					You may have to click many times to see all of the possible outcomes!
					<br/><br/>
				</td>
			</tr>
			 
			<tr>
				<td>
					<?php
					echo "<a id='imagesID' href='javascript:flipcard("."\"".$_SESSION['deckOrder']['E']."\"".")' onClick='recordClicks();'>
						<img class='imgClass' src='images/RandomDeck".$_SESSION['deckOrder']['E'].".jpg' alt='cardback' /></a>";
					// Deck E: 100% chance of $3 (EXPERIENCED)
					?>
				</td>
			</tr>  
			
			<tr>
				<td>
				</td>
			</tr>
			
			<tr>
				<td>
					<input type=button id="pbutton" name="pbutton" value="Previous" class="formButtons" onClick="save_and_move()" disabled />
					<input type=button id="nbutton" name="nbutton" value="Next" class="formButtons" onClick="save_and_move2()" disabled />
					<input type=hidden id='clicks' name='clicks' value=0 />
					<input type=hidden id='previous' name='previous' value=0 />
					<input type=hidden id='next' name='next' value=0 />
				</td>
			</tr>
			
		</tbody>
	</table>
	</form>

	<script language="javascript" type="text/javascript">
		history.forward();
		if(minClickPerDeck == 0){
			document.getElementById('pbutton').disabled=false;
			document.getElementById('nbutton').disabled=false;
		} else if(clickPerDeckCompleted >= minClickPerDeck) {
			document.getElementById('pbutton').disabled=false;
			document.getElementById('nbutton').disabled=false;
		} else {
			document.getElementById('pbutton').disabled=true;
			document.getElementById('nbutton').disabled=true;
		}
	</script>

</body>

</html>
