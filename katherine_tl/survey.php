<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<?php
	require "includes/initialize_inc.php";
	$showError = false;
	
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		$dA4 = ($_POST['DeckA4'] == '0') ? '0' : ( (empty($_POST['DeckA4'])) ? '-1' : $_POST['DeckA4'] );
		$dA0 = ($_POST['DeckA0'] == '0') ? '0' : ( (empty($_POST['DeckA0'])) ? '-1' : $_POST['DeckA0'] );
		$dB3 = ($_POST['DeckB3'] == '0') ? '0' : ( (empty($_POST['DeckB3'])) ? '-1' : $_POST['DeckB3'] );
		$dB0 = ($_POST['DeckB0'] == '0') ? '0' : ( (empty($_POST['DeckB0'])) ? '-1' : $_POST['DeckB0'] );
		$participantID = (empty($_POST['ParticipantID'])) ? '-1' : $_POST['ParticipantID'];
		
		if(intval($dA0) != 80 || intval($dA4) != 20 || intval($dB0) != 75 || intval($dB3) != 25){
			$showError = true;
			$sql = "INSERT INTO ".$_SESSION['tablePrefix']."tl_survey SET p_id=".$_SESSION['p_id'].
				", p_serial=".$_SESSION['p_serial'].", dateTime=now()".
				", DeckADollar4Chance=".$dA4.", DeckADollar0Chance=".$dA0.
				", DeckBDollar3Chance=".$dB3.", DeckBDollar0Chance=".$dB0.
				", ParticipantID='".$participantID."'";
			mysql_query($sql) or die(mysql_error());
		} else {
			$sql = "INSERT INTO ".$_SESSION['tablePrefix']."tl_survey SET p_id=".$_SESSION['p_id'].
				", p_serial=".$_SESSION['p_serial'].", dateTime=now()".
				", DeckADollar4Chance=".$dA4.", DeckADollar0Chance=".$dA0.
				", DeckBDollar3Chance=".$dB3.", DeckBDollar0Chance=".$dB0.
				", ParticipantID='".$participantID."'";
			mysql_query($sql) or die(mysql_error());
?>
			<script type="text/javascript">
				location.href='testingStart.php';
			</script>
<?php 
			exit;
		}
	}
?>

<head>
	<script type="text/javascript" src="js/jquery-1.7.js"></script>
	
	<script type="text/javascript">
		function onlyNumbers(evt) {
			var e = evt; // for trans-browser compatibility
			var charCode = e.which || e.keyCode;
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
			return true;
		}
	</script>
</head>

<body>

<form name="mainform" method="post">
	<table class="outerTableLayout" style="padding:4em;">

<?php 
		if($showError) {	
?>
		<tr class="trOdd" style="padding:2em;"><td>
			<table>
				<tr><td colspan="2">
					<strong>
						Sorry, one or more of the probabilities you entered was not correct.  
						Please review the two decks again and answer the questions below.
					</strong>	
				</td></tr>
				<tr class="trOdd" style="padding:2em;"> 
					<td width="40px"> 
						<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['A']?>.jpg">
					</td>
					<td valign="top"> 
						<strong>
							Deck A: </strong>
							<br/>
							20% chance of $4
							<br/>
			               	80% chance of $0
			        
					</td>
				</tr>
				<tr class="trOdd" style="padding:2em;"> 
					<td width="40px"> 
						<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['B']?>.jpg">
					</td>
					<td valign="top"> 
						<strong>
							Deck B: </strong>
							<br/>
							25% chance of $3
							<br/>
							75% chance of $0
			        	
					</td>
				</tr>
			</table>
		</tr>
<?php 
		}
?>

		<tr><td>
			<strong>A quick reminder before you move on: </strong>	
		</td></tr>
		
		<tr class="trOdd" style="padding:2em;"><td>
			1. For the decks you just saw, what is the probability of getting <b>$4</b> card in <u>Deck <b>A</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['A']?>.jpg">
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckA4" maxlength="3" onkeypress="return onlyNumbers(event);" />
			<br/>
			<i>Only numbers may be entered in this field</i>	
		</td></tr>
		
		<tr class="trOdd"><td>
			2. What is the probability of getting <b>$0</b> card in <u>Deck <b>A</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['A']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckA0" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>
		
		<tr class="trOdd"><td>
			3. What is the probability of getting <b>$3</b> card in <u>Deck <b>B</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['B']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckB3" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>

		<tr class="trOdd"><td>
			4. What is the probability of getting <b>$0</b> card in <u>Deck <b>B</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['B']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckB0" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>
		
		
		<tr class="trEven"><td>
			<br/>
			<input type="hidden" id="next" name="next" value="0">
			<input type="button" id="nbutton" name="next" class="formButtons" value="Next" onclick="save_and_move2()" disabled>
		</td></tr>
		
	</table>
</form>

<script type="text/javascript">
	history.forward();
	document.getElementById('nbutton').disabled=false;
</script>

</body>

</html>