<script language="javascript" type="text/javascript">

function save_and_move() {
	document.getElementById('previous').value = 999;
	setTimeout("document.mainform.submit()", 300);
	return true;
}

function save_and_move2() {
	document.getElementById('next').value = 999;
	setTimeout("document.mainform.submit()", 300);
	return true;
}

function clickCheck() {
	document.getElementById('nbutton').disabled = false;
}

function showCardFront() {
	var newHTML = "<img class='imgClass' src='images/"+cardOrder[(count-1)%cardOrder.length]+".jpg' alt='cardfront' />";
	document.getElementById('imagesID').innerHTML = newHTML;
	if(clickPerDeckCompleted >= minClickPerDeck){
		document.getElementById('pbutton').disabled=false;
		document.getElementById('nbutton').disabled=false;
	} else if(count >= minClickPerDeck){
		document.getElementById('pbutton').disabled=false;
		document.getElementById('nbutton').disabled=false;
	} else {
		document.getElementById('pbutton').disabled=true;
		document.getElementById('nbutton').disabled=true;
	}
}

function flipcard(color){
	var cardBacks = 'images/RandomDeck' + color + '.jpg';
	var newHTML = "<img class='imgClass' src='"+cardBacks+"' alt='cardback' />";
	document.getElementById('imagesID').innerHTML = newHTML;
	setTimeout("showCardFront()", 400);
}

var count = 0;
function recordClicks() {
	count++;
	document.getElementById('clicks').value = count;
}

function decision2() {
	var elements = document.getElementsByTagName('a');
	for(var i=0; i < elements.length; i++) {
		if (elements[i].id == 'DeckA') {
			var newHTML = "<img src='images/DeckA.jpg' class='redborder' alt='selected' />";
			document.getElementById('DeckA').innerHTML = newHTML;
			return;
		}
		if (elements[i].id == 'DeckB') {
			var newHTML = "<img src='images/DeckB.jpg' class='redborder' alt='selected' />";
			document.getElementById('DeckB').innerHTML = newHTML;
		}
		if (elements[i].id == 'DeckC') {
			var newHTML = "<img src='images/DeckC.jpg' class='redborder' alt='selected' />";
			document.getElementById('DeckC').innerHTML = newHTML;
		}
		if (elements[i].id == 'DeckD') {
			var newHTML = "<img src='images/DeckD.jpg' class='redborder' alt='selected' />";
			document.getElementById('DeckD').innerHTML = newHTML;
		}
		if (elements[i].id == 'DeckE') {
			var newHTML = "<img src='images/DeckE.jpg' class='redborder' alt='selected' />";
			document.getElementById('DeckE').innerHTML = newHTML;
		}
	}
}

function radioButtonClick(rbID, leftDeck, rightDeck){
	var des1 = document.getElementById("decision").value;
	var newUnCheckImg = "<img class='imgClassRadioButton' src='images/rb-uncheck.png' alt='"+rbID+"'>";
	var newCheckImg = "<img class='imgClassRadioButton' src='images/rb-check.png' alt='"+rbID+"'>";
	if( (des1 == leftDeck && rbID == "leftRB") || (des1 == rightDeck && rbID == "rightRB")){
		return;
	}
	document.getElementById('leftRB').innerHTML = newUnCheckImg;
	document.getElementById('rightRB').innerHTML = newUnCheckImg;
	document.getElementById(rbID).innerHTML = newCheckImg;
	if(rbID == "leftRB"){
		document.getElementById("decision").value = leftDeck;
	} else if (rbID == "rightRB") {
		document.getElementById("decision").value = rightDeck;
	}
	
	if(document.getElementById("decision").value != 0){
		document.getElementById('nbutton').disabled = false;
	}
}

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	}
	else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode == 13) {
		save_and_move2();
	}
    return true;
}

</script>