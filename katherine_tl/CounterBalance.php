<?php

class CounterBalance{
	
	private static $instance;
	
	/*
	 * counter balance value (choosen randomly)
	 */
	private $counterBalance;	
	
	/*
	 * counter balance groups. 
	 */
	private $allCounterBalanceGroups = array("A1"=>array("A-B", "E-A", "B-D", "B-E", "A-D", "D-E"),
											 "A2"=>array("B-A", "A-E", "D-B", "E-B", "D-A", "E-D"),
										  	 "A3"=>array("D-E", "A-D", "B-E", "B-D", "E-A", "A-B"),
										  	 "A4"=>array("E-D", "D-A", "E-B", "D-B", "A-E", "B-A"),
										  	 "B1"=>array("",""),
										  	 "B2"=>array("",""),
										  	 "B3"=>array("",""),
										  	 "B4"=>array("",""),
										  	 "C1"=>array("A-B", "E-A", "B-D", "B-E", "A-D", "D-E"),
											 "C2"=>array("B-A", "A-E", "D-B", "E-B", "D-A", "E-D"),
										  	 "C3"=>array("D-E", "A-D", "B-E", "B-D", "E-A", "A-B"),
										  	 "C4"=>array("E-D", "D-A", "E-B", "D-B", "A-E", "B-A")
										  	 );

	/*
	 * specify the groups for which the thoughts should be listed.
	 * starts at 0.
	 * "A"=>(0,5) means that the goups at 0th and 5th locations at 
	 * $allCounterBalanceGroups[A*] will have thoughts listed.
	 * for e.g. "A"=>(0,5) means $allCounterBalanceGroups[A1][0] and 
	 * $allCounterBalanceGroups[A1][5] will have thoughts listed. 
	 */
	private $counterBalanceGroupsWithThoughts = array("A"=>array(4,5),
														"B"=>array(1,4),
														"C"=>array(1,2),
													 );										  	 
										  	 
	private function __construct(){ 
		$this->counterBalance = mt_rand(1, 4);		
	}
	
	// Getter method for creating/returning the single instance of this class
	public final static function getInstance() {
		if(!self::$instance) {
			self::$instance = new CounterBalance();
		}
		return self::$instance;
	}
	
	
	public function getCounterBalanceValue() {
		return $this->counterBalance;
	}
	
	public function generateRandomCounterBalanceGroup($version) {
		$temp = $version.$this->counterBalance;
		return $this->allCounterBalanceGroups[$temp];
	}
	
	public function getCounterBalanceGroupsWithThoughts($version){
		return $this->counterBalanceGroupsWithThoughts[$version];
	}
	
	public static function isThoughtsNeededForRound($groupsWithThoughts, $roundNumber){
		foreach($groupsWithThoughts as $value) {
			if($value == $roundNumber) {
				return 1;
			}
		}
		return 0;
	}
	
}

?>