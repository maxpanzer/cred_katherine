# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.5.16-log
# Server OS:                    Win64
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2012-01-28 18:34:23
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table katherine.pd_tl_2_practice
CREATE TABLE IF NOT EXISTS `pd_tl_2_practice` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `number` int(20) NOT NULL,
  `duration` int(20) NOT NULL,
  `input` varchar(500) NOT NULL,
  `practice` varchar(20) NOT NULL,
  PRIMARY KEY (`uin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

# Data exporting was unselected.


# Dumping structure for table katherine.pd_tl_2_responses
CREATE TABLE IF NOT EXISTS `pd_tl_2_responses` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `decks` varchar(20) NOT NULL,
  `duration` int(20) NOT NULL,
  `thoughtNumber` int(20) NOT NULL,
  `thought` varchar(200) NOT NULL,
  `input` varchar(500) NOT NULL,
  `primarily` varchar(20) NOT NULL,
  `valence` varchar(20) NOT NULL,
  `dateTime2` datetime NOT NULL,
  `duration2` int(20) NOT NULL,
  PRIMARY KEY (`uin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

# Data exporting was unselected.


# Dumping structure for table katherine.pd_tl_participants
CREATE TABLE IF NOT EXISTS `pd_tl_participants` (
  `p_id` int(20) NOT NULL AUTO_INCREMENT,
  `p_serial` int(20) NOT NULL,
  `p_server_info` varchar(500) NOT NULL,
  `p_timestarted` datetime NOT NULL,
  `p_timeended` datetime NOT NULL,
  `p_total_time` int(20) NOT NULL,
  `p_payout_round` int(20) NOT NULL,
  `p_payout_decision` varchar(20) NOT NULL,
  `p_payout_amount` int(20) NOT NULL,
  `version` varchar(20) NOT NULL,
  UNIQUE KEY `uin` (`p_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

# Data exporting was unselected.


# Dumping structure for table katherine.pd_tl_practice
CREATE TABLE IF NOT EXISTS `pd_tl_practice` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `round` int(20) NOT NULL,
  `decks` varchar(20) NOT NULL,
  `clicks` int(20) NOT NULL,
  `duration` int(20) DEFAULT NULL,
  `button` varchar(20) NOT NULL,
  KEY `uin` (`uin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table katherine.pd_tl_responses
CREATE TABLE IF NOT EXISTS `pd_tl_responses` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `decks` varchar(20) NOT NULL,
  `duration` int(20) NOT NULL,
  `thoughtNumber` int(20) NOT NULL,
  `input` varchar(500) NOT NULL,
  `primarily` varchar(20) NOT NULL,
  `valence` varchar(20) NOT NULL,
  `dateTime2` datetime NOT NULL,
  `duration2` int(20) NOT NULL,
  PRIMARY KEY (`uin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table katherine.pd_tl_survey
CREATE TABLE IF NOT EXISTS `pd_tl_survey` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `DeckADollar4Chance` int(3) DEFAULT NULL,
  `DeckBDollar0Chance` int(3) DEFAULT NULL,
  `DeckDDollar0Chance` int(3) DEFAULT NULL,
  `DeckDDollar3Chance` int(3) DEFAULT NULL,
  `DeckEDollar0Chance` int(3) DEFAULT NULL,
  `DeckEDollar3Chance` int(3) DEFAULT NULL,
  `DeckEDollar4Chance` int(3) DEFAULT NULL,
  `ParticipantID` int(10) DEFAULT NULL,
  PRIMARY KEY (`uin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

# Data exporting was unselected.


# Dumping structure for table katherine.tl_2_practice
CREATE TABLE IF NOT EXISTS `tl_2_practice` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `number` int(20) NOT NULL,
  `duration` int(20) NOT NULL,
  `input` varchar(500) NOT NULL,
  `practice` varchar(20) NOT NULL,
  PRIMARY KEY (`uin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table katherine.tl_2_responses
CREATE TABLE IF NOT EXISTS `tl_2_responses` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `decks` varchar(20) NOT NULL,
  `duration` int(20) NOT NULL,
  `thoughtNumber` int(20) NOT NULL,
  `thought` varchar(200) NOT NULL,
  `input` varchar(500) NOT NULL,
  `primarily` varchar(20) NOT NULL,
  `valence` varchar(20) NOT NULL,
  `dateTime2` datetime NOT NULL,
  `duration2` int(20) NOT NULL,
  PRIMARY KEY (`uin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

# Data exporting was unselected.


# Dumping structure for table katherine.tl_participants
CREATE TABLE IF NOT EXISTS `tl_participants` (
  `p_id` int(20) NOT NULL AUTO_INCREMENT,
  `p_serial` int(20) NOT NULL,
  `p_server_info` varchar(500) NOT NULL,
  `p_timestarted` datetime NOT NULL,
  `p_timeended` datetime NOT NULL,
  `p_total_time` int(20) NOT NULL,
  `p_payout_round` int(20) NOT NULL,
  `p_payout_decision` varchar(20) NOT NULL,
  `p_payout_amount` int(20) NOT NULL,
  `version` varchar(20) NOT NULL,
  UNIQUE KEY `uin` (`p_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table katherine.tl_practice
CREATE TABLE IF NOT EXISTS `tl_practice` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `round` int(20) NOT NULL,
  `decks` varchar(20) NOT NULL,
  `clicks` int(20) NOT NULL,
  `duration` int(20) DEFAULT NULL,
  `button` varchar(20) NOT NULL,
  KEY `uin` (`uin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

# Data exporting was unselected.


# Dumping structure for table katherine.tl_responses
CREATE TABLE IF NOT EXISTS `tl_responses` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `groups` int(20) NOT NULL,
  `counterbalance` int(20) NOT NULL,
  `round` int(20) NOT NULL,
  `decks` varchar(20) NOT NULL,
  `decision` varchar(20) NOT NULL,
  `judgmentDuration` int(20) NOT NULL,
  `button` varchar(20) NOT NULL,
  PRIMARY KEY (`uin`),
  KEY `uin` (`uin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table katherine.tl_survey
CREATE TABLE IF NOT EXISTS `tl_survey` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `DeckADollar4Chance` int(3) DEFAULT NULL,
  `DeckADollar0Chance` int(3) DEFAULT NULL,
  `DeckBDollar3Chance` int(3) DEFAULT NULL,
  `DeckBDollar0Chance` int(3) DEFAULT NULL,
  `ParticipantID` int(10) DEFAULT NULL,
  PRIMARY KEY (`uin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

# Data exporting was unselected.
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
