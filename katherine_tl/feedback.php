<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<?php
	require "includes/initialize_inc.php";
	$showError = false;
	
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		$dA4 = ($_POST['DeckA4'] == '0') ? '0' : ( (empty($_POST['DeckA4'])) ? '-1' : $_POST['DeckA4'] );
		$dB3 = ($_POST['DeckB3'] == '0') ? '0' : ( (empty($_POST['DeckB3'])) ? '-1' : $_POST['DeckB3'] );
		$dD4 = ($_POST['DeckA4'] == '0') ? '0' : ( (empty($_POST['DeckD4'])) ? '-1' : $_POST['DeckD4'] );
		$dD3 = ($_POST['DeckA0'] == '0') ? '0' : ( (empty($_POST['DeckD3'])) ? '-1' : $_POST['DeckD3'] );
		$dD0 = ($_POST['DeckB3'] == '0') ? '0' : ( (empty($_POST['DeckD0'])) ? '-1' : $_POST['DeckD0'] );
		$dE4 = ($_POST['DeckE4'] == '0') ? '0' : ( (empty($_POST['DeckE4'])) ? '-1' : $_POST['DeckE4'] );
		$dE3 = ($_POST['DeckE0'] == '0') ? '0' : ( (empty($_POST['DeckE3'])) ? '-1' : $_POST['DeckE3'] );
		$dE0 = ($_POST['DeckE3'] == '0') ? '0' : ( (empty($_POST['DeckE0'])) ? '-1' : $_POST['DeckE0'] );
		$participantID = (empty($_POST['ParticipantID'])) ? '-1' : $_POST['ParticipantID'];
		
		$sql = "INSERT INTO ".$_SESSION['tablePrefix']."tl_feedback SET p_id=".$_SESSION['p_id'].
			", p_serial=".$_SESSION['p_serial'].", dateTime=now()".
			", DeckADollar4Chance=".$dA4.", DeckBDollar3Chance=".$dB3.
			", DeckDDollar4Chance=".$dD4.", DeckDDollar3Chance=".$dD3.
			", DeckDDollar0Chance=".$dD0.", DeckEDollar4Chance=".$dE4.
			", DeckEDollar3Chance=".$dE3.", DeckEDollar0Chance=".$dE0.
			", ParticipantID='".$participantID."'";
		mysql_query($sql) or die(mysql_error());
?>
		<script type="text/javascript">
			location.href = "https://vlab.decisionsciences.columbia.edu/referrer/V2_resolver.php";
		</script>
<?php 
			exit;
	}
?>

<head>
	<script type="text/javascript" src="js/jquery-1.7.js"></script>
	
	<script type="text/javascript">
		function onlyNumbers(evt) {
			var e = evt; // for trans-browser compatibility
			var charCode = e.which || e.keyCode;
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
			return true;
		}
	</script>
</head>

<body>

<form name="mainform" method="post">
	<table class="outerTableLayout" style="padding:4em;">

		<tr><td>
			<strong>Final Survey</strong>	
		</td></tr>
		<tr><td>
			Before you move on to the final, short section of this study, please answer these questions about the decks you've been playing with.  If you're not sure about any of the answers, please give us your best guess, or your "gut feeling." 	
		</td></tr>		


		<tr class="trOdd" style="padding:2em;"><td>
			1. As well as you can remember, what was the probability of getting <b>$4</b> card in <u>Deck <b>A</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining']['A']?>.jpg">
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckA4" maxlength="3" onkeypress="return onlyNumbers(event);" />
			<br/>
			<i>Only numbers may be entered in this field</i>	
		</td></tr>
		
		<tr class="trOdd"><td>
			2. What was the probability of getting <b>$3</b> card in <u>Deck <b>B</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining']['B']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckB3" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>
		
		<tr class="trOdd"><td>
			3. What was the probability of getting <b>$4</b> card in <u>Deck <b>D</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining']['D']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckD4" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>

		<tr class="trOdd"><td>
			4. What was the probability of getting <b>$3</b> card in <u>Deck <b>D</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining']['D']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckD3" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>
		
		<tr class="trOdd"><td>
			5. What was the probability of getting <b>$0</b> card in <u>Deck <b>D</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining']['D']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckD0" maxlength="3" onkeypress="return onlyNumbers(event);" />
			
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>

		<tr class="trOdd"><td>
			6. What was the probability of getting <b>$4</b> card in <u>Deck <b>E</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining']['E']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckE4" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>

		<tr class="trOdd"><td>
			7. What was the probability of getting <b>$3</b> card in <u>Deck <b>E</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining']['E']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckE3" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>


	<tr class="trOdd"><td>
			8. What was the probability of getting <b>$0</b> card in <u>Deck <b>E</b></u>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrderTraining']['E']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckE0" maxlength="3" onkeypress="return onlyNumbers(event);" />
			
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>


	
		<tr class="trEven"><td>
			<br/>
			<input type="hidden" id="next" name="next" value="0">
			<input type="button" id="nbutton" name="next" class="formButtons" value="Next" onclick="save_and_move2()" disabled>
		</td></tr>
		
	</table>
</form>

<script type="text/javascript">
	history.forward();
	document.getElementById('nbutton').disabled=false;
</script>

</body>

</html>