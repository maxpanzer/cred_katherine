<?php

class Thoughts{
	
	private static $instance;
	
	private $thoughts = array();
	
	private function __construct() {}
	
	// Getter method for creating/returning the single instance of this class
	public static function getInstance() {
		echo "<script> alert(".isset(self::$instance)."); </script>";
        if (!isset(self::$instance)) {
        	self::$instance = new Thoughts;
        }
        return self::$instance;
	}
	
	function setThought($index, $thought) {
		$this->thoughts[$index] = $thought;
	}
	
	function getAllThoughts(){
		return $this->thoughts;
	}
	
	
	
}

?>