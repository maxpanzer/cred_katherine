<?php require "includes/session_inc.php"; ?>

<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<?php 
	 	require "includes/initialize_inc.php";
//	var_dump($_SESSION);
	
	$round = 2;
	$deck = 'b';
	
	if (array_key_exists('previous', $_POST) && $_POST['previous'] > 0) {
		$clicks = $_POST['clicks'];
		$button = "previous";
		$sql = "SELECT * FROM ".$_SESSION['tablePrefix']."tl_practice WHERE p_id=".$_SESSION['p_id'].
				" and round=".$round." and decks='".$deck."'";
		$result = mysql_query($sql) or die(mysql_error());
		if (mysql_num_rows($result) )
		{
			$row = mysql_fetch_array($result);
			$clicks += $row['clicks'];
			$duration = (time()-$_SESSION['timestart']) + $row['duration'];
			$sql = "UPDATE ".$_SESSION['tablePrefix']."tl_practice SET clicks=".$clicks.", duration='".
					$duration."' WHERE p_id=".$_SESSION['p_id']." and round=".$round." and decks='".$deck."'";
			mysql_query($sql);
		}
		else
		{
		    $sql = "INSERT INTO ".$_SESSION['tablePrefix']."tl_practice SET button = '".$button."', p_id=".
		    		$_SESSION['p_id'].", p_serial=".$_SESSION['p_serial'].
		    		", dateTime=now(), round=$round, decks='$deck', clicks=$clicks, duration='".(time()-$_SESSION['timestart'])."'";
			mysql_query($sql);
		}
		?>
		<script>
			window.location.href="training2.php";
		</script>
		<?php
		exit;
	}
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		$clicks = $_POST['clicks'];
		$button = "next";
		$sql = "SELECT * FROM ".$_SESSION['tablePrefix']."tl_practice WHERE p_id=".$_SESSION['p_id'].
				" and round=".$round." and decks='".$deck."'";
		$result = mysql_query($sql) or die(mysql_error());
		if (mysql_num_rows($result) )
		{
			$row = mysql_fetch_array($result);
			$clicks += $row['clicks'];
			$duration = (time()-$_SESSION['timestart']) + $row['duration'];
			$sql = "UPDATE ".$_SESSION['tablePrefix']."tl_practice SET clicks=".$clicks.", duration='".
					$duration."' WHERE p_id=".$_SESSION['p_id']." and round=".$round." and decks='".$deck."'";
			mysql_query($sql);
		}
		else
		{
		    $sql = "INSERT INTO ".$_SESSION['tablePrefix']."tl_practice SET button = '".$button."', p_id=".
		    		$_SESSION['p_id'].", p_serial=".$_SESSION['p_serial'].
		    		", dateTime=now(), round=$round, decks='$deck', clicks=$clicks, duration='".(time()-$_SESSION['timestart'])."'";
			mysql_query($sql);
		}
		?>
		<script>
			window.location.href="training4.php";
		</script>
		<?php
		exit;
	}
	
	// Start recording time when page opens.
	$_SESSION['timestart'] = time();
	
	?>
	
<head>
</head>

<body>
	
	<form id="mainform" name="mainform" method="post">
	<table class="outerTableLayout" align="center">
		<tbody class="deckLayout">
			<tr>
				<td style="text-align:justify">
					Here is Deck B.
				</td>
			</tr>
			 
			<tr>
				<td>
					<img class="imgClass" src='images/RandomDeck<?php echo $_SESSION['deckOrder']['B']?>.jpg' alt='cardback' />
				</td>
			</tr>  
			
			<tr>
				<td>
					25% chance of a $3 card
					<br/>
					75% chance of a $0 card
					<br/><br/>
				</td>
			</tr>
			
			<tr>
				<td>
					<input type=button id="pbutton" name="pbutton" value="Previous" class="formButtons" onClick="save_and_move()" disabled />
					<input type=button id="nbutton" name="nbutton" value="Next" class="formButtons" onClick="save_and_move2()" disabled />
					<input type=hidden id='clicks' name='clicks' value=0 />
					<input type=hidden id='previous' name='previous' value=0 />
					<input type=hidden id='next' name='next' value=0 />
				</td>
			</tr>
			
		</tbody>
	</table>
	</form>

	<script language="javascript" type="text/javascript">
		history.forward();
		document.getElementById('pbutton').disabled=false;
		document.getElementById('nbutton').disabled=false;
	</script>

</body>

</html>

