<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<?php 
 	require "includes/initialize_inc.php";
	$_SESSION['thoughtIndex'] = 0;
	$_SESSION['thoughts'] = array();
?>

<head>
</head>

<body>

<form id="mainform" name="mainform" method="post">
	<table class="outerTableLayout" align="center">
		<tbody class="deckLayout">
			<tr><td style="text-align:justify">
				<center><h3>Practice Listing Thoughts</h3></center>
			</td></tr>
			<tr>
				<td style="text-align:justify">
					Our goal in this survey is to understand how you make decisions. 
					To help us do this, we will be asking you at several points in the 
					survey to type out your thoughts as you make various choices.
					<br/><br/>
					To make this easy for you, we will be dividing each thought-listing 
					task into a series of screens. To enter your thoughts, all you have 
					to do on each screen is start typing, then hit the enter key when you are done. 
					It's that simple. Again, to tell us what you are thinking, just start typing and 
					then hit the enter key when you are done.
					<br/><br/>
				</td>
			</tr>
			 
			<tr>
				<td>
				</td>
			</tr>  
			
			<tr>
				<td>
 				</td>
			</tr>
			
			<tr>
				<td>
					<input type=button id="nbutton" name="nbutton" value="Next" class="formButtons" onClick="location.href='training9.php'" disabled />
					<input type=hidden id='clicks' name='clicks' value=0 />
					<input type=hidden id='previous' name='previous' value=0 />
					<input type=hidden id='next' name='next' value=0 />
				</td>
			</tr>
			
		</tbody>
	</table>
	</form>

	<script language="javascript" type="text/javascript">
		history.forward();
		document.getElementById('nbutton').disabled=false;
	</script>

</body>

</html>