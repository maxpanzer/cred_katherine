# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.5.16-log
# Server OS:                    Win64
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2012-11-05 07:52:48
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table katherine.pd_participants
CREATE TABLE IF NOT EXISTS `pd_participants` (
  `p_id` int(20) NOT NULL AUTO_INCREMENT,
  `p_serial` int(20) NOT NULL,
  `p_server_info` varchar(500) NOT NULL,
  `p_variant_id` varchar(50) NOT NULL,
  `p_timestarted` datetime NOT NULL,
  `p_timeended` datetime NOT NULL,
  `p_total_time` int(20) NOT NULL,
  `p_payout_round_1` int(20) NOT NULL,
  `p_payout_decision_1` varchar(20) NOT NULL,
  `p_payout_amount_1` int(20) NOT NULL,
  `p_payout_round_2` int(20) NOT NULL,
  `p_payout_decision_2` varchar(20) NOT NULL,
  `p_payout_amount_2` int(20) NOT NULL,
  `p_payout_round_3` int(20) NOT NULL,
  `p_payout_decision_3` varchar(20) NOT NULL,
  `p_payout_amount_3` int(20) NOT NULL,
  `p_payout_round_4` int(20) NOT NULL,
  `p_payout_decision_4` varchar(20) NOT NULL,
  `p_payout_amount_4` int(20) NOT NULL,
  `p_payout_round_5` int(20) NOT NULL,
  `p_payout_decision_5` varchar(20) NOT NULL,
  `p_payout_amount_5` int(20) NOT NULL,
  `p_payout_round_6` int(20) NOT NULL,
  `p_payout_decision_6` varchar(20) NOT NULL,
  `p_payout_amount_6` int(20) NOT NULL,
  UNIQUE KEY `uin` (`p_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table katherine.pd_practice
CREATE TABLE IF NOT EXISTS `pd_practice` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `round` int(20) NOT NULL,
  `decks` varchar(20) NOT NULL,
  `clicks` int(20) NOT NULL,
  `duration` int(20) DEFAULT NULL,
  `button` varchar(20) NOT NULL,
  KEY `uin` (`uin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table katherine.pd_responses
CREATE TABLE IF NOT EXISTS `pd_responses` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `dateTime` datetime NOT NULL,
  `groups` int(20) NOT NULL,
  `counterbalance` int(20) NOT NULL,
  `round` int(20) NOT NULL,
  `decks` varchar(20) NOT NULL,
  `decision` varchar(20) NOT NULL,
  `judgmentDuration` int(20) NOT NULL,
  `button` varchar(20) NOT NULL,
  PRIMARY KEY (`uin`),
  KEY `uin` (`uin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

# Data exporting was unselected.


# Dumping structure for table katherine.pd_survey
CREATE TABLE IF NOT EXISTS `pd_survey` (
  `uin` int(20) NOT NULL AUTO_INCREMENT,
  `p_id` int(20) NOT NULL,
  `p_serial` int(20) NOT NULL,
  `ParticipantID` varchar(20) DEFAULT NULL,
  `dateTime` datetime NOT NULL,
  `DeckD1Dollar4Chance` int(3) DEFAULT NULL,
  `DeckD2Dollar0Chance` int(3) DEFAULT NULL,
  `DeckE1Dollar0Chance` int(3) DEFAULT NULL,
  `DeckE1Dollar3Chance` int(3) DEFAULT NULL,
  `DeckE2Dollar0Chance` int(3) DEFAULT NULL,
  `DeckE2Dollar3Chance` int(3) DEFAULT NULL,
  `DeckE2Dollar4Chance` int(3) DEFAULT NULL,
  PRIMARY KEY (`uin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

# Data exporting was unselected.
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
