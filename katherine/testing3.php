<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >

<html>

	<?php 
	require "includes/initialize_inc.php";
	
	$groups = $_SESSION['groups'];
	$counterbalance = $_SESSION['counterbalance'];
	$round = 2;
	//echo $groups;
	//echo $counterbalance;
//	var_dump($_SESSION);
	
	if ($counterbalance == 1) {
		$decks = 'E2-D1';
		$left = 'E2';
		$leftText = "<br/><br/>";
		$right = 'D1';
		if ($groups == 1) {
			$rightText = "<br/><br/>";
		}
		if ($groups == 2) {
			$rightText = "<br/>80% chance of a $4 card<br/>20% chance of a $0 card";
		}
	}
	
	if ($counterbalance == 2) {
		$decks = 'D1-E2';
		$left = 'D1';
		if ($groups == 1) {
			$leftText = "<br/><br/>";
		}
		if ($groups == 2) {
			$leftText = "<br/>80% chance of a $4 card<br/>20% chance of a $0 card";
		}
		$right = 'E2';
		$rightText = "<br/><br/>";
	}
	
	if ($counterbalance == 3) {
		$decks = 'D1-E1';
		$left = 'D1';
		if ($groups == 1) {
			$leftText = "<br/><br/>";
		}
		if ($groups == 2) {
			$leftText = "<br/>80% chance of a $4 card<br/>20% chance of a $0 card";
		}
		$right = 'E1';
		$rightText = "<br/><br/>";
	}
	
	if ($counterbalance == 4) {
		$decks = 'E1-D1';
		$left = 'E1';
		$leftText = "<br/><br/>";
		$right = 'D1';
		if ($groups == 1) {
			$rightText = "<br/><br/>";
		}
		if ($groups == 2) {
			$rightText = "<br/>80% chance of a $4 card<br/>20% chance of a $0 card";
		}
	}
	
	$decision='null';
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		$decision=$_POST['decision'];		
	}
	
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		$clicks = $_POST['clicks'];
		$sql = "INSERT INTO ".$_SESSION['tablePrefix']."responses SET p_id=".$_SESSION['p_id'].", p_serial=".$_SESSION['p_serial'].", dateTime=now(), groups=$groups, counterbalance=$counterbalance, round=$round, decks='$decks', decision='$decision', judgmentDuration='".(time()-$_SESSION['timestart'])."', button = 'next'";
		mysql_query($sql);
	?>
		<script>
			window.location.href="testing4.php";
		</script>
	<?php
		exit;
	}
	
	// Start recording time when page opens.
	$_SESSION['timestart'] = time();
	
	$rightText="";
	$leftText="";
	?>

<head>
</head>

<body>
	
<form id="mainform" name="mainform" method="post">
	<table class="outerTableLayout" align="center">
		<tbody class="deckLayout">
			<tr>
				<td style="text-align:justify">
					<?php echo $round ?>. Please choose one deck to pull a card from: 
				</td>
			</tr>
			
			<tr>
				<td style="text-align:justify">
				<table cellspacing='10' align="center">
					<tr>
						<td align='center'><img class="imgClass" src="images/RandomDeck<?php echo $_SESSION['deckOrder'][$left] ?>.jpg" alt='cardback' /></td>
						<td><B>OR</B></td>
						<td align='center'><img class="imgClass" src="images/RandomDeck<?php echo $_SESSION['deckOrder'][$right] ?>.jpg" alt='cardback' /></td>
					</tr>
					
					
					<tr valign="middle" align="center">
						<td>
							<b>Deck <?php echo Deck::getDeckNameFromId($left) ?></b>
							<a id="leftRB" href="javascript:radioButtonClick('leftRB','<?php echo $left ?>','<?php echo $right ?>')">
								<img class="imgClassRadioButton" src="images/rb-uncheck.png" alt="RadioButtonImage">
							</a>
						</td>
						<td></td>
						<td>
							<b>Deck <?php echo Deck::getDeckNameFromId($right) ?></b>
							<a id="rightRB" href="javascript:radioButtonClick('rightRB','<?php echo $left ?>','<?php echo $right ?>')">
								<img class="imgClassRadioButton" src="images/rb-uncheck.png" alt="RadioButtonImage">
							</a>
						</td>
					</tr>
				</table>
				</td>
			</tr>					

			<tr>
				<td>
					<input type=hidden id='clicks' name='clicks' value=0 />
					<input type=hidden id='next' name='next' value=0 />
					<input type=hidden id='decision' name='decision' value=0 />
					<input type=button id="nbutton" name="nbutton" value="Next" class="formButtons" onClick="save_and_move2()" disabled />
				</td>
			</tr>
		</tbody>
	</table>
</form>
	
	<script language="javascript" type="text/javascript">
		history.forward();
		document.getElementById('nbutton').disabled=true;
		document.getElementById('nbutton').style.visibility='hidden';
	</script>
	
</body>

</html>
