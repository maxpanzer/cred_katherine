<?php
/* This is the global config file for the whole script. When you edit the 
config variables here, be careful about adding double quotes ("). They must
always be preceded by a backslash (\), or else there will be a parse error.  */
global $CFG;

//MySQL database connection info
/*
 * katherine's database properties on vLab server
 * $CFG['db_host'] = "localhost";
 * $CFG['db_user'] = "katherinedb";
 * $CFG['db_pass'] = "b9absak2d";
 * $CFG['db_database'] = "katherine";
 */
$CFG['db_host'] = "localhost";
$CFG['db_user'] = "root";
$CFG['db_pass'] = "";
$CFG['db_database'] = "katherine";

$CFG['isPD'] = true;

//require SSL?
$CFG['require_https'] = false;

//version information
$CFG['variant_id'] = "variant_1";

//the root dir. If all the files are in the cct directory in the web root of your server, these settings should be fine
/*
 * changed the following line to make the web root point to the internal location on local system.
 * $CFG['web_root'] = "https://".$_SERVER['HTTP_HOST']."/act_exp/katherine/";
 */
$CFG['web_root'] = "http://".$_SERVER['HTTP_HOST']."/CRED_Katherine/katherine/";
	
/*
 * changed the following line to make the document root point to the internal location on local system.
 * $CFG['doc_root'] = $_SERVER['DOCUMENT_ROOT']."/act_exp/katherine/";
 */
$CFG['doc_root'] = $_SERVER['DOCUMENT_ROOT']."CRED_Katherine/katherine/";

$CFG['title'] = "Decisions"; //default site title

/*
 * changed the floowing for internal server purposes.
 * $CFG['finishpage'] = "https://".$_SERVER['HTTP_HOST']."/katherine/thankYou.php";
 */
$CFG['finishpage'] = "http://".$_SERVER['HTTP_HOST']."/CRED_Katherine/katherine/thankYou.php";


/*
 * Cards Configuration. 
 * Give the Order of the cards for Each Deck here. This will be used in determining the order in which the
 * cards are shown to the user. Kind a like a crude puseudo-random cards order generator. 
 * The final pay to the user will also depend on the values given here.
 */
$CFG['cardsOrder']['cardOrderOfDeckD1'] = "4,0,0,0,0";
$CFG['cardsOrder']['cardOrderOfDeckD2'] = "3";
$CFG['cardsOrder']['cardOrderOfDeckX1'] = "4,3,0,0,0,0,0";
$CFG['cardsOrder']['cardOrderOfDeckE1'] = "4,0,0,0,0";
$CFG['cardsOrder']['cardOrderOfDeckE2'] = "3";

/* atleast one of the following two should be -1. 
 * if atleast one is not -1, then minimumNumCicksPerDeck will be considered */
$CFG['minimumNumCicksPerDeck'] = "-1";
$CFG['exactNumCicksPerDeck'] = "3";

/* Below two are not yet implemented */
$CFG['showDescriptionDecksDuringTraining'] = "";
$CFG['showDescriptionOfDescriptionDecksDuringTesting'] = "";

$CFG['maxDecksChoosenForPayout'] = "6";

?>