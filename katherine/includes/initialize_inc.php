<?php 
//set php values
//error_reporting(E_ALL);
//ini_set("error_reporting",1);
//ini_set("max_execution_time",10);

//set up basic functionality: config, db, functions, classes...
require "config_inc.php";
require $CFG['doc_root']."/Deck.php";

if($CFG['isPD'] == true){
	$_SESSION['isPD'] = true;
	$_SESSION['tablePrefix'] = "pd_";	
} else {
	$_SESSION['isPD'] = false;
	$_SESSION['tablePrefix'] = "";
}

include $CFG['doc_root']."/includes/db_connect_inc.php";
include $CFG['doc_root']."/js/javascript_inc.php"; 
include $CFG['doc_root']."/css/css_inc.php";

// always modified
// header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

//make sure they are secure
if ($CFG['require_https'] == true && $_SERVER['HTTPS'] != true) {
	echo 'Secure connection required.';
	exit;
}

//detect old browsers
$unsupported_browsers = array("msie 5","msie 4","msie 3","netscape 5","netscape 4","netscape 3");
foreach ($unsupported_browsers as $browser) {
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), $browser) !== false) { 
	?><script>
	window.location.href="oldbrowser.php";
	</script><?php
	exit;
	}
}

//save their serial ID and UID if they have one
if (isset($_GET['serial']) && !isset($_SESSION['p_serial'])) {
	$_SESSION['p_serial'] = $_GET['serial'];
	$sql = "INSERT INTO ".$_SESSION['tablePrefix']."participants SET p_serial='".$_SESSION['p_serial']."', p_variant_id='".$CFG['variant_id']."', p_server_info=\"".addslashes(serialize($_SERVER))."\", p_timestarted=now()";
	mysql_query($sql);
	$_SESSION['p_id'] = mysql_insert_id();
} elseif (!isset($_GET['serial']) && !isset($_SESSION['p_serial'])) {
	$_SESSION['p_serial'] = 999;
	$sql = "INSERT INTO ".$_SESSION['tablePrefix']."participants SET p_serial='".$_SESSION['p_serial']."', p_variant_id='".$CFG['variant_id']."', p_server_info=\"".addslashes(serialize($_SERVER))."\", p_timestarted=now()";
	mysql_query($sql);
	$_SESSION['p_id'] = mysql_insert_id();
}

if(!isset($_SESSION['isDeckInitialized']) || !$_SESSION['isDeckInitialized']){
	$cardsOrderLength = array();
	$CFG['cardsOrder']['cardOrderOfDeckD1'] = preg_split('/,/', $CFG['cardsOrder']['cardOrderOfDeckD1']);
	$CFG['cardsOrder']['cardOrderOfDeckD2'] = preg_split('/,/', $CFG['cardsOrder']['cardOrderOfDeckD2']);
	$CFG['cardsOrder']['cardOrderOfDeckX1'] = preg_split('/,/', $CFG['cardsOrder']['cardOrderOfDeckX1']);
	$CFG['cardsOrder']['cardOrderOfDeckE1'] = preg_split('/,/', $CFG['cardsOrder']['cardOrderOfDeckE1']);
	$CFG['cardsOrder']['cardOrderOfDeckE2'] = preg_split('/,/', $CFG['cardsOrder']['cardOrderOfDeckE2']);
	$_SESSION['cardsOrderFromConfig'] = $CFG['cardsOrder'];
}

$_SESSION['minimumNumCicksPerDeck'] = $CFG['minimumNumCicksPerDeck'];
$_SESSION['exactNumCicksPerDeck'] = $CFG['exactNumCicksPerDeck'];

$_SESSION['maxDecksChoosenForPayout'] = $CFG['maxDecksChoosenForPayout']; 

?>