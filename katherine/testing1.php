<?php require "includes/session_inc.php"; ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<?php 
	require "includes/initialize_inc.php";
	
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		//mysql_query($sql);
	?>
		<script>
			window.location.href="testing2.php";
		</script>
	<?php
		exit;
	}
	
	$_SESSION['groups'] = mt_rand(1,2);
	$_SESSION['thankYou'] = 0;
	$_SESSION['counterbalance'] = mt_rand(1,4);
	$_SESSION['reward'] = mt_rand(0,4);
	
	?>

<head>
</head>

<body>

<form id="mainform" name="mainform" method="post">
	<table class="outerTableLayout" align="center">
		<tbody class="deckLayout">
			<tr>
				<td style="text-align:justify">
					You will next see some pairs of cards, and you will be able to choose one card from each pair. The values on the backs of each card you select will be added to your point total.  
					<br/><br/>
					
					<br/><br/>
					You will find out the value of the cards you selected once you have made all six choices.
					<br/><br/>
				</td>
			</tr>
			<tr>
				<td>
					<input type=hidden id='clicks' name='clicks' value=0 />
					<input type=hidden id = 'next' name='next' value=0 />
					<input type=button id="nbutton" name="nbutton" value="Next" class="formButtons" onClick="save_and_move2()" disabled />
				</td>
			</tr>
		</tbody>
	</table>
</form>
	
	<br/><br/>
	<script language="javascript" type="text/javascript">
		history.forward();
		document.getElementById('nbutton').disabled=false;
	</script>

</body>

</html>
