<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<html>

	<?php 
	require "includes/initialize_inc.php";
	
	$round = 3;
	$deck = 'X1';
	
	if (array_key_exists('previous', $_POST) && $_POST['previous'] > 0) {
		$clicks = $_POST['clicks'];
		$button = "previous";
		$sql = "SELECT * FROM ".$_SESSION['tablePrefix']."practice WHERE p_id=".$_SESSION['p_id']." and round=".$round." and decks='".$deck."'";
		$result = mysql_query($sql) or die(mysql_error());
		if (mysql_num_rows($result) )
		{
			$row = mysql_fetch_array($result);
			$clicks += $row['clicks'];
			$duration = (time()-$_SESSION['timestart']) + $row['duration'];
			$sql = "UPDATE ".$_SESSION['tablePrefix']."practice SET clicks=".$clicks.", duration='".$duration."' WHERE p_id=".$_SESSION['p_id']." and round=".$round." and decks='".$deck."'";
			mysql_query($sql);
		}
		else
		{
		    $sql = "INSERT INTO ".$_SESSION['tablePrefix']."practice SET button = '".$button."', p_id=".$_SESSION['p_id'].", p_serial=".$_SESSION['p_serial'].", dateTime=now(), round=$round, decks='$deck', clicks=$clicks, duration='".(time()-$_SESSION['timestart'])."'";
			mysql_query($sql);
		}
		?>
		<script>
			window.location.href="training3.php";
		</script>
		<?php
		exit;
	}
	
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		$clicks = $_POST['clicks'];
		$button = "next";
		$sql = "SELECT * FROM ".$_SESSION['tablePrefix']."practice WHERE p_id=".$_SESSION['p_id']." and round=".$round." and decks='".$deck."'";
		$result = mysql_query($sql) or die(mysql_error());
		if (mysql_num_rows($result) )
		{
			$row = mysql_fetch_array($result);
			$clicks += $row['clicks'];
			$duration = (time()-$_SESSION['timestart']) + $row['duration'];
			$sql = "UPDATE ".$_SESSION['tablePrefix']."practice SET clicks=".$clicks.", duration='".$duration."' WHERE p_id=".$_SESSION['p_id']." and round=".$round." and decks='".$deck."'";
			mysql_query($sql);
		}
		else
		{
		    $sql = "INSERT INTO ".$_SESSION['tablePrefix']."practice SET button = '".$button."', p_id=".$_SESSION['p_id'].", p_serial=".$_SESSION['p_serial'].", dateTime=now(), round=$round, decks='$deck', clicks=$clicks, duration='".(time()-$_SESSION['timestart'])."'";
			mysql_query($sql);
		}
		?>
		<script>
			window.location.href="training5.php";
		</script>
		<?php
		exit;
	}
	
	// Start recording time when page opens.
	$_SESSION['timestart'] = time();
	
	?>

<head>
	<?php 
		echo Deck::getInstance()->getCardOrderJavaScriptForRound($round);
	?>
	<script language="javascript" type="text/javascript">
		var round = "<?php echo $round; ?>";
	</script>
</head>

<body>

<form id="mainform" name="mainform" method="post">
	<table class="outerTableLayout" align="center">
		<tbody class="deckLayout">
			<tr>
				<td style="text-align:justify">
					Here is Deck C.
					<br/><br/>
					Click on it to test out the cards it gives you.
					<br/><br/>
					You may click as many times as you want, until you feel confident that you know how good Deck C is. 
					You may have to click many times to see all of the possible outcomes!
					<br/><br/>
				</td>
			</tr>
			
			<tr>
				<td align="right" style="text-align:center;">
		 			<div id="sourceImageDiv">
			 			<img id="sourceImage" class='imgClass' src='images/RandomDeck<?php echo $_SESSION['deckOrder'][$deck]?>.jpg' 
							onclick="makeCardAppearAndSlideOut()" alt='cardback'/>
		 			</div>
	 			</td>
			</tr>
			
			<tr>
				<td>
				</td>
			</tr>
			
			<tr>
				<td>
					<input type=button id="nbutton" name="nbutton" value="Next" 
							class="formButtons" onClick="save_and_move2()" disabled />
					<input type=hidden id='clicks' name='clicks' value=0 />
					<input type=hidden id='previous' name='previous' value=0 />
					<input type=hidden id='next' name='next' value=0 />
				</td>
			</tr>
			
		</tbody>
	</table>
	</form>

	<script language="javascript" type="text/javascript">
		history.forward();
		if(minClickPerDeck == 0 || exactClickPerDeck == 0) {
			document.getElementById('nbutton').disabled=false;
			document.getElementById('nbutton').style.visibility='visible';
		} else {
			document.getElementById('nbutton').disabled=true;
			document.getElementById('nbutton').style.visibility='hidden';
		}
	</script>

</body>

</html>

