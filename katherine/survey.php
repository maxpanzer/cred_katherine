<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >

<html>

<?php
	require "includes/initialize_inc.php";
	
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
		$dD1_4 = (empty($_POST['DeckD1_4'])) ? '-1' : $_POST['DeckD1_4'];
		$dD2_0 = (empty($_POST['DeckD2_0'])) ? '-1' : $_POST['DeckD2_0'];
		$dE1_0 = (empty($_POST['DeckE1_0'])) ? '-1' : $_POST['DeckE1_0'];
		$dE1_3 = (empty($_POST['DeckE1_3'])) ? '-1' : $_POST['DeckE1_3'];
		$dE2_0 = (empty($_POST['DeckE2_0'])) ? '-1' : $_POST['DeckE2_0'];
		$dE2_3 = (empty($_POST['DeckE2_3'])) ? '-1' : $_POST['DeckE2_3'];
		$dE2_4 = (empty($_POST['DeckE2_4'])) ? '-1' : $_POST['DeckE2_4'];
		$participantID = (empty($_POST['ParticipantID'])) ? '-1' : $_POST['ParticipantID'];
		
		$sql = "INSERT INTO ".$_SESSION['tablePrefix']."survey SET p_id=".$_SESSION['p_id'].", p_serial=".$_SESSION['p_serial'].", dateTime=now()".
			", DeckD1Dollar4Chance=".$dD1_4.", DeckD2Dollar0Chance=".$dD2_0.
			", DeckE1Dollar0Chance=".$dE1_0.", DeckE1Dollar3Chance=".$dE1_3.
			", DeckE2Dollar0Chance=".$dE2_0.", DeckE2Dollar3Chance=".$dE2_3.
			", DeckE2Dollar4Chance=".$dE2_4.", ParticipantID='".$participantID."'";
		mysql_query($sql) or die(mysql_error());
?>
		<script type="text/javascript">
			location.href='closeWindow.php';
		</script>
<?php 
		exit;
	}
?>

<head>
	<script type="text/javascript">
		function onlyNumbers(evt) {
			var e = evt; // for trans-browser compatibility
			var charCode = e.which || e.keyCode;
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
			return true;
		}
	</script>
</head>

<body>

<form name="mainform" method="post">
	<table class="outerTableLayout" style="padding:4em;">
		<tr><td>
			<strong>A Quick Check of Your Memory</strong>	
		</td></tr>
		
		<tr class="trOdd" style="padding:2em;"><td>
			1. In Deck <b><u><?php echo Deck::getDeckNameFromId("D1") ?></u></b>, what was the chance of getting a <b>$4</b> card? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['D1']?>.jpg">
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckD1_4" maxlength="3" onkeypress="return onlyNumbers(event);" />
			<br/>
			<i>Only numbers may be entered in this field</i>	
		</td></tr>
		
		<tr class="trOdd"><td>
			2. In Deck <b><u><?php echo Deck::getDeckNameFromId("D2") ?></u></b>, what was the chance of getting a <b>$0</b> card? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['D2']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckD2_0" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>
		
		<tr class="trOdd"><td>
			3. Based on your experience, what was the chance of getting a <b>$0</b> card in Deck <b><u><?php echo Deck::getDeckNameFromId("E1") ?></u></b>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['E1']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckE1_0" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>
		
		<tr class="trOdd"><td>
			4. Based on your experience, what was the chance of getting a <b>$3</b> card in Deck <b><u><?php echo Deck::getDeckNameFromId("E1") ?></u></b>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['E1']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckE1_3" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>
		
		<tr class="trOdd"><td>
			5. Based on your experience, what was the chance of getting a <b>$0</b> card in Deck <b><u><?php echo Deck::getDeckNameFromId("E2") ?></u></b>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['E2']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckE2_0" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>
		
		<tr class="trOdd"><td>
			6. Based on your experience, what was the chance of getting a <b>$3</b> card in Deck <b><u><?php echo Deck::getDeckNameFromId("E2") ?></u></b>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['E2']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckE2_3" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>
		
		<tr class="trOdd"><td>
			7. Based on your experience, what was the chance of getting a <b>$4</b> card in Deck <b><u><?php echo Deck::getDeckNameFromId("E2") ?></u></b>? ____%
			<br/>
			<img src="images/RandomDeck<?php echo $_SESSION['deckOrder']['E2']?>.jpg">	
		</td></tr>
		<tr class="trEven"><td>
			<input class="textBox" type="text" name="DeckE2_4" maxlength="3" onkeypress="return onlyNumbers(event);" />	
			<br/>
			<i>Only numbers may be entered in this field</i>
		</td></tr>
		

		
		<tr class="trEven"><td>
			<br/>
			<input type="hidden" id="next" name="next" value="0">
			<input type="button" id="nbutton" name="next" class="formButtons" value="Next" onclick="save_and_move2()" disabled>
		</td></tr>
	
		
		<tr>
				
		</tr>
		
	</table>
</form>

<script type="text/javascript">
	history.forward();
	document.getElementById('nbutton').disabled=false;
</script>

</body>

</html>