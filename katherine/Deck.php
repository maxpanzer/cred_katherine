<?php
/**
 * Deck is a singleton class, ie, At any given time, only one instance of Deck object is present <br/>
 * use Deck::getInstance() to get the instance of Deck
 */
class Deck {
	
	private static $m_pInstance;
	
	private $currentDeck = 0;
	
	private $allDecks = array("D1", "D2", "X1", "E1", "E2");
	private static $deckIdToNameMap = array (
		"D1" => "A",
		"D2" => "B",
		"X1" => "C",
		"E1" => "D",
		"E2" => "E"
	);
	private $allRandomDeckImages = array('Red', 'Blue', 'Purple', 'Orange', 'Green');
	
	private $deckMap = array();
	private $cardOrderMap = array();
	
	private function __construct() { }

	// Getter method for creating/returning the single instance of this class
	public static function getInstance() {
		if (!self::$m_pInstance)
		{
			self::$m_pInstance = new Deck();
		}
		return self::$m_pInstance;
	}
	
	function generateRandomdeckImagesOrder() {
		for ($i = count($this->allRandomDeckImages)-1; $i >= 0; $i--) {
			$j = mt_rand(0, $i);
			$temp = $this->allRandomDeckImages[$i];
			$this->allRandomDeckImages[$i] = $this->allRandomDeckImages[$j];
			$this->allRandomDeckImages[$j] = $temp;
		}	
	}
	
	function getRandomDeckImagesOrder() {
		$toReturn = "allRandomDeckImages = ";
		for ($i = 0; $i < count($this->allRandomDeckImages); $i++) {
			$toReturn = $toReturn.$this->allRandomDeckImages[$i].", ";
		}
		$toReturn = $toReturn."<br/> allDecks = ";
		for ($i = 0; $i < count($this->allRandomDeckImages); $i++) {
			$toReturn = $toReturn.$this->allDecks[$i]."-".$this->deckMap[$this->allDecks[$i]].", ";
		}
		return $toReturn;
	}	
	
	function generateDeckMap() {
	 	$this->generateRandomdeckImagesOrder();
		for ($i = 0; $i < count($this->allRandomDeckImages); $i++) {
			$this->deckMap[$this->allDecks[$i]] = $this->allRandomDeckImages[$i];
		}
	}
	
	function getDeckmap() {
		return $this->deckMap;
	}
	
	function getCurrentDeck() {
		return $this->currentDeck;
	}
	
	function initializeCardOrderMap() {
		$_SESSION['isDeckInitialized'] = TRUE;
		while (list($key, $value) = each($_SESSION['cardsOrderFromConfig'])) {
			$_SESSION['cardsOrderFromConfig'][$key] = $this->generateRandomOrder($value);
		}
	}
	
	function generateRandomOrder($array) {
		for ($i = count($array)-1; $i >= 0; $i--) {
			$j = mt_rand(0, $i);
			$temp = $array[$i];
			$array[$i] = $array[$j];
			$array[$j] = $temp;
		}
		return $array;
	}
	
	function getCardOrderJavaScriptForRound($round){
		$tmpParameter = "cardOrderOfDeck".$this->allDecks[$round-1];
		$cardOrdetTemp = $_SESSION['cardsOrderFromConfig'][$tmpParameter];
		// create script string to append to content. First create the value array in JavaScript.
		$script = "" . "\n" . '<script language="javascript" type="text/javascript"> '. "\nvar cardOrder = new Array(";
		if (count($cardOrdetTemp) > 1) {
		    foreach ($cardOrdetTemp as $key => $value) {
		        if ($key < (count($cardOrdetTemp)-1)) {
		            $script = $script . $value . ',';
		        } else {
		            $script = $script . $value . ");\n";
		        }
		    }
		}
		else {  // can't create an array that just has one integer element using Array()
		    $script = $script . "1); \n cardOrder[0]=" . $cardOrdetTemp[0] . ";\n";
		}
		$script = $script . "var exactClickPerDeck=" .$_SESSION['exactNumCicksPerDeck'] .";\n";
		$script = $script . "var minClickPerDeck=" .$_SESSION['minimumNumCicksPerDeck'] .";\n".'</script>'."\n";
		return $script;
	}
	
	static function getDeckNameFromId($id) {
		return self::$deckIdToNameMap[$id];
	}
	
}

?>