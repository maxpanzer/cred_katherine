<?php require "includes/session_inc.php"; ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<?php 
require "includes/initialize_inc.php";

//we will check this value on the next page.
$_SESSION['cookietest'] = true;
$_SESSION['taskStartTime'] = time();

Deck::getInstance()->generateDeckMap();
Deck::getInstance()->initializeCardOrderMap();

$_SESSION['deckOrder'] = Deck::getInstance()->getDeckmap();
?>
<!DOCTYPE center PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<script type="text/javascript" src="js/jquery-1.8.2.js"></script>
	<script type="text/javascript">
		function goToNextPage(){
			location.href = 'training2.php';
		}
	</script>
</head>

<body>

	<table class="outerTableLayout" align="center">
		<tbody><tr><td>
		
			<strong>Card Flip</strong>
	
				<br/><br/>
			Welcome to Card Flip!  This game will take about 10 minutes to complete and you will perform better if you play it all in one sitting, so please make sure you have 10 minutes free before you begin. 
			<br/><br/>
			You're about to start the first phase of the game, where you will learn about some different decks of cards.  Each card is worth a certain amount of points. 
			<br/><br/>
			The decks may differ in the <i>point values</i> of the cards they contain, or <i>how many</i> of each value of card they contain, or both.  
			<br/><br/>
			Later on, you will select cards from the different decks, and your score in this game will depend on the point values on the cards you selected, so <font color="red"><b>the better you remember how good each deck is, the more points you are likely to earn!</b></font> 
			You may spend as much time as you want getting familiar with the decks, but please <b>do not write any deck information down</b> as you do.
			
			<br/><br/>
			
		 	<form>
				<table width=100%>
					<tr>
						<td align=center width=100%>
							<input type=button id="nbutton" value="Next" class="formButtons" 
								onClick="goToNextPage()" disabled />
						</td>
					</tr>
				</table>
			</form>

		</td></tr></tbody>
	</table>
	
	<script language="javascript" type="text/javascript">
		history.forward();
		document.getElementById('nbutton').disabled=false;
	</script>
	
</body>

</html>

