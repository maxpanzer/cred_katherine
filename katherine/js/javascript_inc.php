<script language="javascript" src="js/jquery-1.8.2.js"></script>

<script language="javascript" type="text/javascript">

var forceMoveToNextRound = false;

function save_and_move() {
	document.getElementById('previous').value = 999;
	setTimeout("document.mainform.submit()", 300);
	return true;
}

function save_and_move2() {
	document.getElementById('next').value = 999;
	setTimeout("document.mainform.submit()", 300);
	return true;
}

function clickCheck() {
	document.getElementById('nbutton').disabled = false;
}

var count = 0;
function recordClicks() {
	count++;
	document.getElementById('clicks').value = count;
}

function showCardFront() {
	var newHTML = "<img class='imgClass' src='images/"+cardOrder[(count-1)%cardOrder.length]+".jpg' alt='cardfront' />";
	document.getElementById('imagesID').innerHTML = newHTML;
}

function flipcard(color){
	var cardBacks = 'images/RandomDeck' + color + '.jpg';
	var newHTML = "<img class='imgClass' src='"+cardBacks+"' alt='cardback' />";
	document.getElementById('imagesID').innerHTML = newHTML;
	setTimeout("showCardFront()", 400);
}

function checkClicksPerDeck() {
	if (minClickPerDeck != -1) {
		return true;
	} else if (exactClickPerDeck != -1) {
		if (count == exactClickPerDeck){
			return false;
		} else {
			return true;
		}
	}
}

function enableOrDisableNextButton() {
	if (minClickPerDeck != -1) {
		if (count >= minClickPerDeck){
			enableNextButton(true);
			makeXpirenceDeckVisible(false);
		} else {
			enableNextButton(false);
		}
	} else if (exactClickPerDeck != -1) {
		if (count == exactClickPerDeck){
			enableNextButton(true);
			makeXpirenceDeckVisible(false);
		} else {
			enableNextButton(false);
		}
	}
}

function makeXpirenceDeckVisible(isVisible) {
	if (!isVisible) {
		$('#sourceImage').css('visibility', 'hidden');
	} else {
		$('#sourceImage').css('visibility', 'visible');
	}
}

function enableNextButton(isEnable) {
	if (isEnable == true && document.getElementById('nbutton') != null) {
		document.getElementById('nbutton').disabled=false;
		document.getElementById('nbutton').style.visibility='visible';
	} else if (document.getElementById('nbutton') != null) {
		document.getElementById('nbutton').disabled=true;
		document.getElementById('nbutton').style.visibility='hidden';
	}
}


function makeCardAppearAndSlideOut() {
	var checkClass = $('#sourceImageDiv').hasClass('processingClick');
	if (checkClass == true) {
		return;
	}
	
	if (!checkClicksPerDeck()) {
		enableNextButton(true);
		alert("You have exceeded the number of clicks, please click Next Button to continue.");
		return;
	}
	
	$('#sourceImageDiv').addClass("processingClick");
	recordClicks();
	if(document.getElementById('clonedImage') != null) {
		$('#clonedImage').remove();
	}
	var replaceImg = 'images/'+cardOrder[(count-1)%cardOrder.length]+'.jpg';
	$('#sourceImageDiv img')
	.clone(false, false)
	.attr('id', 'clonedImage')
	.prependTo('#sourceImageDiv')
	.css({'position' : 'absolute'})
	.animate({opacity: 0.8}, 100 )
	.animate({"left": "+=300px"}, 1500)
	.animate({opacity: 1}, 100, 
		function(){
			$(this).attr('src', replaceImg);
			enableOrDisableNextButton();
			$('#sourceImageDiv').removeClass("processingClick");
		}
	);
}

function decision2() {
	var elements = document.getElementsByTagName('a');
	for(var i=0; i < elements.length; i++) {
		if (elements[i].id == 'DeckA') {
			var newHTML = "<img src='images/DeckA.jpg' class='redborder' alt='selected' />";
			document.getElementById('DeckA').innerHTML = newHTML;
			return;
		}
		if (elements[i].id == 'DeckB') {
			var newHTML = "<img src='images/DeckB.jpg' class='redborder' alt='selected' />";
			document.getElementById('DeckB').innerHTML = newHTML;
		}
		if (elements[i].id == 'DeckC') {
			var newHTML = "<img src='images/DeckC.jpg' class='redborder' alt='selected' />";
			document.getElementById('DeckC').innerHTML = newHTML;
		}
		if (elements[i].id == 'DeckD') {
			var newHTML = "<img src='images/DeckD.jpg' class='redborder' alt='selected' />";
			document.getElementById('DeckD').innerHTML = newHTML;
		}
		if (elements[i].id == 'DeckE') {
			var newHTML = "<img src='images/DeckE.jpg' class='redborder' alt='selected' />";
			document.getElementById('DeckE').innerHTML = newHTML;
		}
	}
}

function radioButtonClick(rbID, leftDeck, rightDeck){
	var des1 = document.getElementById("decision").value;
	var newUnCheckImg = "<img class='imgClassRadioButton' src='images/rb-uncheck.png' alt='"+rbID+"'>";
	var newCheckImg = "<img class='imgClassRadioButton' src='images/rb-check.png' alt='"+rbID+"'>";
	if( (des1 == leftDeck && rbID == "leftRB") || (des1 == rightDeck && rbID == "rightRB")){
		return;
	}
	document.getElementById('leftRB').innerHTML = newUnCheckImg;
	document.getElementById('rightRB').innerHTML = newUnCheckImg;
	document.getElementById(rbID).innerHTML = newCheckImg;
	if(rbID == "leftRB") {
		document.getElementById("decision").value = leftDeck;
	} else if (rbID == "rightRB") {
		document.getElementById("decision").value = rightDeck;
	}
	
	if(document.getElementById("decision").value != 0){
		document.getElementById('nbutton').disabled = false;
		document.getElementById('nbutton').style.visibility='visible'
	}
}

</script>