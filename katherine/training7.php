<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<html>

	<?php 
	require "includes/initialize_inc.php";
	$_SESSION['cookietest'] = true;
	
	if (array_key_exists('previous', $_POST) && $_POST['previous'] > 0) {
	?>
		<script>
			window.location.href="training6.php";
		</script>
	<?php
		exit;
	}
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
	?>
		<script>
			window.location.href="testing1.php";
		</script>
	<?php
		exit;
	}
	?>

<head>
</head>

<body>

<form id="mainform" name="mainform" method="post">
	<table class="outerTableLayout" align="center">
		<tbody class="deckLayout">
			<tr>
				<td style="text-align:justify">
					<center><h3></h3></center>
					<strong>Moving on!</strong>
					<br/><br/>
					Before you play the game for points, you'll have the chance to test your memory about some of the decks you just saw.
	
				</td>
			</tr>
			 
			<tr>
				<td>
				</td>
			</tr>  
			
			<tr>
				<td>
 				</td>
			</tr>
			
			<tr>
				<td>
					<input type=button id="nbutton" name="nbutton" value="Next" 
							class="formButtons" onClick="save_and_move2()" disabled />
					<input type=hidden id='clicks' name='clicks' value=0 />
					<input type=hidden id='previous' name='previous' value=0 />
					<input type=hidden id='next' name='next' value=0 />
				</td>
			</tr>
			
		</tbody>
	</table>
	</form>

	<script language="javascript" type="text/javascript">
		history.forward();
		document.getElementById('nbutton').disabled=false;
	</script>

</body>

</html>
