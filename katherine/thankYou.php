<?php require "includes/session_inc.php"; ?>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >

<html>

	<?php require "includes/initialize_inc.php";

	if ($_SESSION['thankYou'] == 0) {
		$maxPayoutRounds = $_SESSION['maxDecksChoosenForPayout'];
		$roundsOrder = array(1,2,3,4,5,6);
		$payoutRounds = array($maxPayoutRounds);
		
		// shuffle the rounds. 
		for ($i = count($roundsOrder)-1; $i >= 0; $i--) {
			$j = mt_rand(0, $i);
			$temp = $roundsOrder[$i];
			$roundsOrder[$i] = $roundsOrder[$j];
			$roundsOrder[$j] = $temp;
		}
		
		// pick random rounds from the rounds order.
		for ($i = 0; $i < $maxPayoutRounds; $i++) {
			$payoutRounds[$i] = $roundsOrder[$i];
		}
		
		// set the text to be displayed in the browser.
		$text = "Your selections for each of the six choices you just made were as follows: \n";
		
		$reward = array();
		$rewardTotal = 0;
		$index = 0;
		$deckNames = array();
		foreach ($payoutRounds as $refPayoutRounds) {
			$sql = "SELECT round, decks, decision FROM ".$_SESSION['tablePrefix']."responses WHERE p_id='"
					.$_SESSION['p_id']."' AND p_serial='".$_SESSION['p_serial']."' AND round=".$refPayoutRounds."";
			$result = mysql_query($sql) or die(mysql_error());
			$row = mysql_fetch_array($result);
			$round = $row['round'];
			$decks[$index] = $row['decks'];
			// convert D1-D2 to A-B for displaying on the screen
			$curDeckNames = explode("-", $decks[$index]);
			$deckNames[$index] = Deck::getDeckNameFromId($curDeckNames[0])."-".Deck::getDeckNameFromId($curDeckNames[1]);
			
			$decision[$index] =  $row['decision'];
			if ($decision[$index] == 'D1') {
				$input = $_SESSION['cardsOrderFromConfig']['cardOrderOfDeckD1'];
				$reward[$index] = $input[mt_rand(0,count($input)-1)];
				//rewardText = '(80% chance of a $4 card, 20 % chance of a $0 card)';
			}
			if ($decision[$index] == 'D2') {
				$input = $_SESSION['cardsOrderFromConfig']['cardOrderOfDeckD2'];
				$reward[$index] = $input[mt_rand(0,count($input)-1)];
				//$rewardText = '(100% chance of a $3 card)';
			}
			if ($decision[$index] == 'X1') {
				$input = $_SESSION['cardsOrderFromConfig']['cardOrderOfDeckX1'];
				$reward[$index] = $input[mt_rand(0,count($input)-1)];
				//$rewardText = '(80% chance of a $3 card, 10% chance of a $4 card, 10% chance of a $0 card)';
			}
			if ($decision[$index] == 'E1') {
				$input = $_SESSION['cardsOrderFromConfig']['cardOrderOfDeckE1'];
				$reward[$index] = $input[mt_rand(0,count($input)-1)];
				//$rewardText = '(80% chance of a $4 card, 20 % chance of a $0 card)';
			}
			if ($decision[$index] == 'E2') {
				$input = $_SESSION['cardsOrderFromConfig']['cardOrderOfDeckE2'];
				$reward[$index] = $input[mt_rand(0,count($input)-1)];
				//$rewardText = '(100% chance of a $3 card)';
			}
			$text = $text . "<p>".($index+1).". <b>Pair".$deckNames[$index]."</b>. \n";
			$text = $text . "In that choice, you selected <b>Deck".Deck::getDeckNameFromId($decision[$index])."</b>.";
			$text = $text . " The card you chose was worth <b>".$reward[$index]." points</b>. </p>";
			$rewardTotal = $rewardTotal + $reward[$index];
			$index++;
		}
		$text = $text . "<p><br/>This means that your score for this round was <b>".$rewardTotal." points</b>.";
		$text = $text . "<br/><br/>Please click <b>Next</b> to move on to the final, short sections of the survey. </p>";
		
		$sql2 = "UPDATE ".$_SESSION['tablePrefix']."participants 
				SET p_timeended=now()
				, p_total_time='".(time()-$_SESSION['taskStartTime'])."'";
		for ($i = 0; $i < count($roundsOrder); $i++) {
			$sql2 = $sql2 . ", p_payout_round_".($i+1)."=".(empty($decision[$i])?-1:$payoutRounds[$i])."
					, p_payout_decision_".($i+1)."='".(empty($decision[$i])?'NA':$decision[$i])."'
					, p_payout_amount_".($i+1)."=".(empty($decision[$i])?-1:$reward[$i])."";	
		}
		$sql2 = $sql2 . " WHERE p_serial='".$_SESSION['p_serial']."' AND p_id='".$_SESSION['p_id']."' ";
		mysql_query($sql2) or die(mysql_error());
		$_SESSION['thankYou'] = 0;
	}
	
	if (array_key_exists('next', $_POST) && $_POST['next'] > 0) {
	?>
		<script>
//			window.location.href="https://vlab.decisionsciences.columbia.edu/referrer/V2_resolver.php";
			window.location.href="survey.php";
		</script>
	<?php
		exit;
	}
	
	?>

<head>
</head>

<body>

<form id="mainform" name="mainform" method="post">
	<table class="outerTableLayout" align="center">
		<tbody class="deckLayout">
			<tr>
				<td style="text-align:justify">
					<?php echo $text; ?>
				</td>
			</tr>

			<tr>
				<td>
					<input type=hidden id='next' name='next' value=0 />
					<input type=button id="nbutton" name="nbutton" value="Next" class="formButtons" 
							onClick="save_and_move2()" disabled />
				</td>
			</tr>
		</tbody>
	</table>
</form>

	<script language="javascript" type="text/javascript">
		history.forward();
		document.getElementById('nbutton').disabled=false;
	</script>

</body>

</html>
